package com.rkfcheung.quoteengine.service

import com.rkfcheung.quoteengine.AbstractTest
import com.rkfcheung.quoteengine.client.FinnhubClient
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired

internal class TickerServiceTest : AbstractTest() {
    @Autowired
    private lateinit var tickerService: TickerService

    @Autowired
    private lateinit var finnhubClient: FinnhubClient

    @Test
    fun testSearch() {
        val code = "4:HK"
        val ticker = tickerService.search(code)
        assertEquals("0004.HK", ticker?.code)
        assertEquals("BBG000BD5H69", ticker?.figiCode())
    }

    @Test
    fun testSearchHSI() {
        val code = "^HSI"
        val ticker = tickerService.search(code)
        assertEquals(code, ticker?.code)
        assertEquals("HSI:IND", ticker?.bloombergCode())
    }

    @Test
    fun testSearchFairwood() {
        val code = "52.HK"
        val ticker = tickerService.search(code)
        log.info("${ticker?.asTickerMeasurement()}")
    }

    @Disabled
    @Test
    fun testFinnhubClient() = runBlocking {
        val symbols = finnhubClient.getStockSymbols("HK")
        symbols.filter { it.symbol.length <= 6 }.collect { s ->
            log.info("$s")
            val ticker = tickerService.search(s.code)
            log.info("${ticker?.asTickerMeasurement()}")
        }
    }
}