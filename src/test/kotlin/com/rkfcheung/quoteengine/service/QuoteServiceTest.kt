package com.rkfcheung.quoteengine.service

import com.opengamma.strata.product.common.ExchangeIds
import com.rkfcheung.quoteengine.AbstractTest
import com.rkfcheung.quoteengine.model.definition.Resolution
import com.rkfcheung.quoteengine.model.quote.YahooQuoteRequest
import com.rkfcheung.quoteengine.model.ticker.EquityTicker
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import java.time.ZonedDateTime

internal class QuoteServiceTest : AbstractTest() {
    @Autowired
    private lateinit var quoteService: QuoteService

    @Test
    fun testQuote() {
        val years = 1L
        val ticker = EquityTicker(ExchangeIds.XHKG, "3")
        val endTime = ZonedDateTime.now()
        val startTime = endTime.minusYears(years)
        val request = YahooQuoteRequest(Resolution.DAILY, startTime, endTime, ticker)
        val prices = quoteService.quote(request)
        assertTrue(prices.isNotEmpty())
        prices.take(8).forEach { p -> log.info("$p") }
    }
}