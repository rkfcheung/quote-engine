package com.rkfcheung.quoteengine.util

import com.opengamma.strata.basics.location.Country
import com.opengamma.strata.product.common.ExchangeId
import com.opengamma.strata.product.common.ExchangeIds
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

internal class MarketMapperTest {

    @Test
    fun testMapCode() {
        assertEquals("0001", MarketMapper.toCode(ExchangeIds.XHKG, "1"))
        assertEquals("0011", MarketMapper.toCode(ExchangeIds.XHKG, "11"))
        assertEquals("0823", MarketMapper.toCode(ExchangeIds.XHKG, "823"))
        assertEquals("2800", MarketMapper.toCode(ExchangeIds.XHKG, "2800"))
    }

    @Test
    fun testAsExchange() {
        assertEquals(MarketMapper.ARCX, MarketMapper.toExchangeIdOrNull("UP"))
        assertEquals(MarketMapper.XASE, MarketMapper.toExchangeIdOrNull("XN"))
        assertEquals(MarketMapper.XBOS, MarketMapper.toExchangeIdOrNull("UB"))
        assertEquals(MarketMapper.XDUB, MarketMapper.toExchangeIdOrNull("ID"))
        assertEquals(MarketMapper.XDUB, MarketMapper.toExchangeIdOrNull("IR"))
        assertEquals(ExchangeIds.XHKG, MarketMapper.toExchangeIdOrNull("HK"))
        assertEquals(MarketMapper.XLUX, MarketMapper.toExchangeIdOrNull("LX"))
        assertEquals(MarketMapper.XNCM, MarketMapper.toExchangeIdOrNull("UR"))
        assertEquals(MarketMapper.XNGS, MarketMapper.toExchangeIdOrNull("UW"))
        assertEquals(MarketMapper.XNIM, MarketMapper.toExchangeIdOrNull("UT"))
        assertEquals(MarketMapper.XNMS, MarketMapper.toExchangeIdOrNull("UQ"))
        assertEquals(MarketMapper.XNYS, MarketMapper.toExchangeIdOrNull("UN"))
        assertEquals(MarketMapper.XPSX, MarketMapper.toExchangeIdOrNull("UX"))
        assertEquals(ExchangeIds.XSES, MarketMapper.toExchangeIdOrNull("XS"))
        assertEquals(ExchangeIds.XSES, MarketMapper.toExchangeIdOrNull("SI"))
        assertEquals(MarketMapper.XSHE, MarketMapper.toExchangeIdOrNull("CS"))
        assertEquals(MarketMapper.XSHE, MarketMapper.toExchangeIdOrNull("SZ"))
        assertEquals(MarketMapper.XSHE, MarketMapper.toExchangeIdOrNull("SHE"))
        assertEquals(MarketMapper.XSHG, MarketMapper.toExchangeIdOrNull("CG"))
        assertEquals(MarketMapper.XSHG, MarketMapper.toExchangeIdOrNull("SS"))
        assertEquals(MarketMapper.XSHG, MarketMapper.toExchangeIdOrNull("SHA"))

        assertEquals(ExchangeIds.XHKG, MarketMapper.toExchangeIdOrNull("Hong Kong"))
        assertEquals(ExchangeIds.XTKS, MarketMapper.toExchangeIdOrNull("Japan"))
        assertEquals(MarketMapper.XKRX, MarketMapper.toExchangeIdOrNull("Korea (South)"))
        assertEquals(MarketMapper.XLUX, MarketMapper.toExchangeIdOrNull("Luxembourg"))
        assertEquals(MarketMapper.XNAS, MarketMapper.toExchangeIdOrNull("NASDAQ GM"))
        assertEquals(null, MarketMapper.toExchangeIdOrNull("null"))
        assertEquals(MarketMapper.ARCX, MarketMapper.toExchangeIdOrNull("NYSE Arca"))
        assertEquals(MarketMapper.XSHG, MarketMapper.toExchangeIdOrNull("Shanghai"))
        assertEquals(MarketMapper.XSHE, MarketMapper.toExchangeIdOrNull("Shenzhen"))
    }

    @Test
    fun testToMarket() {
        Country.getAvailableCountries().forEach {
            if (it == Country.EU) {
                return@forEach
            }
            val market = MarketMapper.toMarket(ExchangeId.of("X${it.code3Char}"))
            when (it) {
                Country.LU -> assertEquals("LX", market)
                else -> assertEquals(it.code, market)
            }
        }
    }
}