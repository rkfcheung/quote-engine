package com.rkfcheung.quoteengine.util

import com.opengamma.strata.basics.currency.Currency
import com.opengamma.strata.product.ProductType
import com.rkfcheung.quoteengine.model.ticker.Ticker
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test

internal class TickerUtilTest {
    @Test
    fun testToBloombergCode() {
        assertEquals("2:HK", YahooUtil.toBloombergCode("0002.HK"))
    }

    @Test
    fun testTickerToBloombergCode() {
        val code = "2800.HK"
        val bbCode = "2800:HK"
        val ticker = Ticker(ProductType.SECURITY, code)
        assertEquals(code, ticker.yahooCode())
        assertEquals(bbCode, YahooUtil.toBloombergCode(code))
        assertEquals(bbCode, ticker.bloombergCode())

        val ticker2 = ticker.add(true)
        assertTrue(ticker2.active() == true)
        assertEquals(ticker.type, ticker2.type)
        assertEquals(bbCode, ticker2.bloombergCode())
        assertEquals(ticker, ticker2)

        ticker2.add(Currency.HKD)
        assertEquals(Currency.HKD, ticker2.currency())
    }
}