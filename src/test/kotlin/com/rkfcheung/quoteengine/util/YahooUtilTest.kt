package com.rkfcheung.quoteengine.util

import com.jimmoores.quandl.Frequency
import com.rkfcheung.quoteengine.util.YahooUtil.asForexPrice
import com.rkfcheung.quoteengine.util.YahooUtil.asInterval
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import yahoofinance.histquotes.Interval
import yahoofinance.quotes.fx.FxQuote
import java.math.BigDecimal

internal class YahooUtilTest {
    @Test
    fun testAsFreqInterval() {
        val intervals = Interval.values().map { it.name }
        Frequency.values().filter { intervals.contains(it.name) }.forEach {
            assertEquals(it.name, it.asInterval().name)
        }
    }

    @Test
    fun testAsForexPrice() {
        val fxQuote = FxQuote("USDHKD=X", BigDecimal.valueOf(7.8))
        val fxPrice = fxQuote.asForexPrice()
        assertEquals(fxQuote.symbol, fxPrice.parameterCode.code)
        assertEquals(fxQuote.price.toDouble(), fxPrice.value)
    }
}