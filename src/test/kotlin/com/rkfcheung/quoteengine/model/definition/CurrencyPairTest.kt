package com.rkfcheung.quoteengine.model.definition

import com.opengamma.strata.basics.currency.CurrencyPair
import com.opengamma.strata.product.ProductType
import com.rkfcheung.quoteengine.util.TickerUtil
import com.rkfcheung.quoteengine.util.YahooUtil
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test

internal class CurrencyPairTest {
    @Test
    fun testGetAvailablePairs() {
        val currencyPairs = CurrencyPair.getAvailablePairs()
        currencyPairs.forEach { p ->
            val ticker = TickerUtil.toForexTickerOrNull(p.toString())
            val bbCode = "${p.base}${p.counter}:CUR"
            val yahooCode = "${p.base}${p.counter}=X"
            assertTrue(ticker != null)
            assertEquals(ProductType.FX_SINGLE, ticker?.type)
            assertEquals(yahooCode, ticker?.code)
            assertEquals(bbCode, ticker?.bloombergCode())
            assertEquals(ticker, TickerUtil.asForexTicker(bbCode))
            assertEquals(ticker, TickerUtil.asForexTicker(yahooCode))
            assertEquals(bbCode, YahooUtil.toBloombergCode(yahooCode))
        }
    }
}