package com.rkfcheung.quoteengine.model.price.measurement

import com.opengamma.strata.data.ObservableSource

object TestPriceUtil {
    const val TEST_CODE = "influx-test"
    const val TEST_MEASUREMENT = "test-price"
    val RANDOM: ObservableSource = ObservableSource.of("Random")
}