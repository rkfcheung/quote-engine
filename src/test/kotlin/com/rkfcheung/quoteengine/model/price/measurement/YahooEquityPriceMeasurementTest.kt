package com.rkfcheung.quoteengine.model.price.measurement

import com.opengamma.strata.product.ProductType
import com.opengamma.strata.product.common.ExchangeIds
import com.rkfcheung.quoteengine.AbstractTest
import com.rkfcheung.quoteengine.client.InfluxPriceClient
import com.rkfcheung.quoteengine.connector.YahooEquityConnector
import com.rkfcheung.quoteengine.model.definition.Resolution
import com.rkfcheung.quoteengine.model.quote.RangeQuoteRequest
import com.rkfcheung.quoteengine.model.quote.YahooQuoteRequest
import com.rkfcheung.quoteengine.model.ticker.EquityTicker
import com.rkfcheung.quoteengine.model.ticker.Ticker
import com.rkfcheung.quoteengine.util.DateTimeUtil.asZonedDateTime
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import java.time.LocalDate
import java.time.ZonedDateTime

internal class YahooEquityPriceMeasurementTest : AbstractTest() {
    @Autowired
    private lateinit var yahooEquityConnector: YahooEquityConnector

    @Autowired
    private lateinit var influxPriceClient: InfluxPriceClient

    @Test
    fun testCopyFromYahooToInflux() {
        val ticker = EquityTicker(ExchangeIds.XHKG, "175")
        val endTime = ZonedDateTime.now()
        val startTime = endTime.minusMonths(1L)
        val request = YahooQuoteRequest(Resolution.DAILY, startTime, endTime, ticker)
        val prices = yahooEquityConnector.quote(request)
        prices.forEach { p -> log.info("$p") }
        influxPriceClient.save(prices)
    }

    @Test
    fun testGetPricesFromInflux() {
        val ticker = EquityTicker(ExchangeIds.XHKG, "175")
        val endTime = ZonedDateTime.now()
        val startTime = endTime.minusMonths(1L)
        val request = YahooQuoteRequest(Resolution.DAILY, startTime, endTime, ticker)
        val prices = influxPriceClient.quote(request)
        prices.forEach { p -> log.info("$p") }
    }

    @Test
    fun testDeleteFromInflux() {
        val ticker = EquityTicker(ExchangeIds.XHKG, "3126")
        val endTime = LocalDate.of(2021, 3, 16).asZonedDateTime()
        val startTime = LocalDate.of(2018, 8, 1).asZonedDateTime()
        val request = YahooQuoteRequest(Resolution.DAILY, startTime, endTime, ticker)
        val prices = influxPriceClient.quote(request)
        prices.forEach { p ->
            log.info("$p")
            if (p.value < 5.0) {
                influxPriceClient.delete(p)
            }
        }

        val testCode = Ticker(ProductType.OTHER, TestPriceUtil.TEST_CODE)
        val now = ZonedDateTime.now()
        influxPriceClient.quote(RangeQuoteRequest(Resolution.NONE, now.minusMonths(1L), now, testCode)).forEach { p ->
            log.info("$p")
            influxPriceClient.delete(p)
        }
    }
}