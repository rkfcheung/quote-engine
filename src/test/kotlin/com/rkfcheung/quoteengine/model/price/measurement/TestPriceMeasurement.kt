package com.rkfcheung.quoteengine.model.price.measurement

import com.influxdb.annotations.Column
import com.influxdb.annotations.Measurement
import com.rkfcheung.quoteengine.model.definition.Resolution
import java.time.Instant
import kotlin.random.Random

@Measurement(name = TestPriceUtil.TEST_MEASUREMENT)
data class TestPriceMeasurement(
    @Column(tag = true) override var code: String = "",
    @Column(timestamp = true) override var time: Instant = Instant.now(),
    @Column override var value: Double = 0.0,
    @Column var bool: Boolean = Random.nextBoolean()
) : PriceMeasurement(
    code,
    time,
    value = value,
    resolution = Resolution.NONE.name,
    parameterSource = TestPriceUtil.RANDOM.name
)
