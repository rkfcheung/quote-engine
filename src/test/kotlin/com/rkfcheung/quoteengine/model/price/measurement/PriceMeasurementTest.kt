package com.rkfcheung.quoteengine.model.price.measurement

import com.influxdb.client.InfluxDBClient
import com.influxdb.client.domain.DeletePredicateRequest
import com.opengamma.strata.data.ObservableSource
import com.opengamma.strata.product.ProductType
import com.rkfcheung.quoteengine.AbstractTest
import com.rkfcheung.quoteengine.client.InfluxPriceClient
import com.rkfcheung.quoteengine.config.InfluxProperties
import com.rkfcheung.quoteengine.model.definition.MeasurementBasics
import com.rkfcheung.quoteengine.model.definition.Resolution
import com.rkfcheung.quoteengine.model.price.SingleValuePrice
import com.rkfcheung.quoteengine.model.quote.RangeQuoteRequest
import com.rkfcheung.quoteengine.model.ticker.Ticker
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import java.time.ZonedDateTime
import kotlin.random.Random

internal class PriceMeasurementTest : AbstractTest() {
    @Autowired
    private lateinit var properties: InfluxProperties

    @Autowired
    private lateinit var influxDBClient: InfluxDBClient

    @Autowired
    private lateinit var influxPriceClient: InfluxPriceClient

    @Test
    fun testAddPoint() = runBlocking {
        val ticker = Ticker(ProductType.OTHER, TestPriceUtil.TEST_CODE)
        val value = Random.nextDouble()
        val now = ZonedDateTime.now()
        val price = SingleValuePrice(Resolution.NONE, TestPriceUtil.RANDOM, now, ticker, value)
        val point = price.asPoint(TestPriceUtil.TEST_MEASUREMENT)
            .addField("bool", Random.nextBoolean())
            .addTag("quoteId", price.quoteId().toString())
            .addTag("standardId", ticker.standardId().toString())
        influxDBClient.writeApi.writePoint(point)
        influxDBClient.writeApi.writeMeasurement(MeasurementBasics.WRITE_PRECISION, price.asMeasurement())
        influxDBClient.writeApi.flush()

        val request = RangeQuoteRequest(price.resolution, now.minusHours(1L), now, ticker)
        val query = influxPriceClient.prepareQuery(request, MeasurementBasics.PRICE, listOf("bool"))
        log.info(query)
        val results = influxDBClient.queryApi.query(query)
        val record = results.map { it.records }.lastOrNull()?.lastOrNull()
        log.info("${record?.values}")
        assertTrue(record?.value != null)

        val prices = influxPriceClient.measurements<TestPriceMeasurement>(request)
        prices.takeLast(8).forEach { p ->
            log.info("${p.asSingleValuePrice()}")
        }
    }

    @Test
    fun testReplaceValue() {
        val ticker = Ticker(ProductType.OTHER, TestPriceUtil.TEST_CODE)
        val value = Random.nextDouble()
        val expectedValue = Math.PI / 10.0
        val price = SingleValuePrice(Resolution.TICK, ObservableSource.NONE, ZonedDateTime.now(), ticker, value)
        val point = price.asPoint(TestPriceUtil.TEST_MEASUREMENT)
            .addTag("className", javaClass.simpleName)
        influxDBClient.writeApi.writePoint(point)
        influxDBClient.writeApi.writePoint(
            point.addField(MeasurementBasics.VALUE, expectedValue)
                .addField("oldValue", value)
        )
        influxDBClient.writeApi.flush()

        val results = influxDBClient.queryApi.query(
            """
from(bucket: "${properties.bucket}")
  |> range(start: -1h)
  |> filter(fn: (r) => r["_measurement"] == "${TestPriceUtil.TEST_MEASUREMENT}")
  |> filter(fn: (r) => r["${MeasurementBasics.PARAMETER_SOURCE}"] == "${price.parameterSource}") 
  |> filter(fn: (r) => r["className"] == "${javaClass.simpleName}")    
  |> filter(fn: (r) => r["_field"] == "${MeasurementBasics.VALUE}") 
  |> yield(name: "last")
            """.trimIndent()
        )
        val record = results.map { it.records }.lastOrNull()?.lastOrNull()
        assertTrue(record != null)
        log.info("${record?.values}")
        assertEquals(expectedValue, record?.value)
        assertEquals(price.parameterSource.name, record?.getValueByKey(MeasurementBasics.PARAMETER_SOURCE))
    }

    @Test
    fun testDeleteTestMeasurements() {
        val now = ZonedDateTime.now()
        val request = DeletePredicateRequest().apply {
            start = now.minusDays(7L).toOffsetDateTime()
            stop = now.toOffsetDateTime()
            predicate = "_measurement=\"${TestPriceUtil.TEST_MEASUREMENT}\" AND bool=\"false\""
        }
        influxDBClient.deleteApi.delete(request, properties.bucket, properties.org)
    }

    @Test
    fun testDeleteWrongMeasurements() {
        val now = ZonedDateTime.now()
        val request = DeletePredicateRequest().apply {
            start = now.minusYears(1L).toOffsetDateTime()
            stop = now.toOffsetDateTime()
            predicate = "_measurement=\"${MeasurementBasics.PRICE}\" AND resolution=\"${ProductType.SECURITY}\""
        }
        influxDBClient.deleteApi.delete(request, properties.bucket, properties.org)
    }
}