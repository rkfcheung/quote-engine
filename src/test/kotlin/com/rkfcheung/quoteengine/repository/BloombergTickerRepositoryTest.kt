package com.rkfcheung.quoteengine.repository

import com.opengamma.strata.product.ProductType
import com.opengamma.strata.product.common.ExchangeIds
import com.rkfcheung.quoteengine.AbstractTest
import com.rkfcheung.quoteengine.util.MarketMapper
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired

internal class BloombergTickerRepositoryTest : AbstractTest() {
    @Autowired
    private lateinit var bloombergTickerRepository: BloombergTickerRepository

    @Test
    fun testFindById() {
        val expected = "5:HK"
        val ticker = bloombergTickerRepository.findById(expected).get()
        log.info(ticker.toString())
        assertEquals(expected, ticker.id)
    }

    @Test
    fun testFindByCode() {
        val expected = "000001.SZ"
        val bloombergTicker = bloombergTickerRepository.findByCode(expected) ?: return
        val ticker = bloombergTicker.asTicker()
        log.info(bloombergTicker.toString())
        log.info(ticker.toString())
        log.info(bloombergTicker.asTickerEntity().toString())
        assertEquals(expected, bloombergTicker.code)
        assertEquals(expected, ticker.yahooCode())
        assertEquals(bloombergTicker.id, ticker.bloombergCode())
    }

    @Test
    fun testFindAll() {
        val tickers = bloombergTickerRepository.findAll()
        tickers.groupBy { it.securityType }.forEach { (type, values) ->
            log.info("$type ${values.size}")
            if (type != "COMMON_STOCK") {
                log.info("${values.map { it.code to it.name }}")
            } else {
                values.filterNot { it.active() }.forEach {
                    assertEquals(ProductType.SECURITY, it.asTicker().type)
                    log.info("[${it.id}] ${it.code} ${it.name} ${it.active()} ${it.market()} ${it.micCode()} ${it.productType()}")
                    if (it.micCode() == ExchangeIds.XHKG) {
                        assertEquals(MarketMapper.HK.code, it.market())
                    }
                }
            }
        }
    }
}