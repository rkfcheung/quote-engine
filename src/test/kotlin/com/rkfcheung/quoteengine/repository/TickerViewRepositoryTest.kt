package com.rkfcheung.quoteengine.repository

import com.opengamma.strata.basics.currency.Currency
import com.rkfcheung.quoteengine.AbstractTest
import com.rkfcheung.quoteengine.model.entity.TickerViewId
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.repository.findByIdOrNull

internal class TickerViewRepositoryTest : AbstractTest() {
    @Autowired
    private lateinit var tickerViewRepository: TickerViewRepository

    @Test
    fun testFindById() {
        val expected = "0001.HK"
        val ticker = tickerViewRepository.findById(TickerViewId(expected, 2)).get()
        log.info(ticker.toString())
        assertEquals(expected, ticker.code)
        assertEquals(2, ticker.assetClassId)
    }

    @Test
    fun testFindByCode() {
        val expected = "0002.HK"
        val ticker = tickerViewRepository.findByIdOrNull(TickerViewId(expected, 2)) ?: return
        log.info(ticker.toString())
        log.info(ticker.asTicker().toString())
        log.info(ticker.asTickerEntity().toString())
        assertEquals(expected, ticker.code)
        assertEquals(Currency.HKD.code, ticker.currency)
        assertEquals("2:HK", ticker.asTicker().bloombergCode())
        assertEquals(expected, ticker.asTicker().yahooCode())
    }

    @Test
    fun testFindAll() {
        val tickers = tickerViewRepository.findAll()
        tickers.groupBy { it.assetClassId }.forEach { (type, values) ->
            log.info("$type ${values.size}")
        }
    }
}