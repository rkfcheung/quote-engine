package com.rkfcheung.quoteengine.repository

import com.opengamma.strata.basics.currency.Currency
import com.opengamma.strata.product.ProductType
import com.rkfcheung.quoteengine.AbstractTest
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.repository.findByIdOrNull

internal class HkExSecurityRepositoryTest : AbstractTest() {
    @Autowired
    private lateinit var hkExSecurityRepository: HkExSecurityRepository

    @Test
    fun testFindById() {
        val expected = "0001.HK"
        val security = hkExSecurityRepository.findById(expected).get()
        log.info(security.toString())
        assertEquals(expected, security.code)
        assertEquals(ProductType.SECURITY, security.productType())
    }

    @Test
    fun testFindByCode() {
        val expected = "0002.HK"
        val security = hkExSecurityRepository.findByIdOrNull(expected) ?: return
        log.info(security.toString())
        log.info(security.asTicker().toString())
        log.info(security.asTickerEntity().toString())
        assertEquals(expected, security.code)
        assertEquals(Currency.HKD, security.currency())
        assertEquals("2:HK", security.asTicker().bloombergCode())
        assertEquals(expected, security.asTicker().yahooCode())
    }

    @Test
    fun testFindAll() {
        val centres = hkExSecurityRepository.findAll()
        centres.groupBy { it.board }.forEach { (type, values) ->
            log.info("$type ${values.size}")
        }
    }
}