package com.rkfcheung.quoteengine.repository

import com.rkfcheung.quoteengine.AbstractTest
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired

internal class AssetClassRepositoryTest : AbstractTest() {
    @Autowired
    private lateinit var assetClassRepository: AssetClassRepository

    @Test
    fun testFindAll() {
        assetClassRepository.findAll().forEach { ac ->
            log.info("$ac -> ${ac.asProductType()}")
        }
    }
}