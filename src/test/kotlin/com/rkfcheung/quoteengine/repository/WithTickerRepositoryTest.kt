package com.rkfcheung.quoteengine.repository

import com.opengamma.strata.product.common.ExchangeIds
import com.rkfcheung.quoteengine.AbstractTest
import com.rkfcheung.quoteengine.client.InfluxPriceClient
import com.rkfcheung.quoteengine.model.entity.TickerViewId
import com.rkfcheung.quoteengine.model.ticker.measurement.TickerMeasurement
import com.rkfcheung.quoteengine.util.YahooUtil
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.repository.findByIdOrNull

internal class WithTickerRepositoryTest : AbstractTest() {
    @Autowired
    private lateinit var assetClassRepository: AssetClassRepository

    @Autowired
    private lateinit var bloombergTickerRepository: BloombergTickerRepository

    @Autowired
    private lateinit var exchangeCentreRepository: ExchangeCentreRepository

    @Autowired
    private lateinit var hkExSecurityRepository: HkExSecurityRepository

    @Autowired
    private lateinit var tickerEntityRepository: TickerEntityRepository

    @Autowired
    private lateinit var tickerViewRepository: TickerViewRepository

    @Autowired
    private lateinit var influxPriceClient: InfluxPriceClient

    @Disabled
    @Test
    fun testFindAll() {
        val tickers = tickerViewRepository.findAll().filter { it.active() } + hkExSecurityRepository.findAll() +
                exchangeCentreRepository.findAll() + bloombergTickerRepository.findAll() + tickerEntityRepository.findAll()
        val tickerMap = mutableMapOf<String, TickerMeasurement>()
        tickers.forEach {
            val code = YahooUtil.toCode(it.code)
            val ticker = it.asTicker()
            val measurement = tickerMap[code] ?: ticker.asTickerMeasurement()
            measurement.update(ticker)
            tickerMap[code] = measurement
        }
        tickerMap.forEach { (code, measurement) ->
            if (measurement.active == false || measurement.asTickerEntity().isMissing()) {
                return@forEach
            }

            val result = influxPriceClient.ticker(code)
            if (result == null) {
                log.info("$code => $measurement")
                influxPriceClient.save(measurement)
            }
        }
    }

    @Test
    fun testAddTickerMeasurement() {
        val code = "0001.HK"
        val bloombergTicker = bloombergTickerRepository.findByCode(code) ?: return
        log.info("$code => $bloombergTicker")
        val ticker = bloombergTicker.asTicker()
        val measurement = ticker.asTickerMeasurement()
        val result = influxPriceClient.ticker(code)
        log.info("$code => $result")
        if (result == null) {
            log.info("$code => $measurement")
            influxPriceClient.save(ticker)
        }
    }

    @Test
    fun testFindOnTickerView() {
        val code = "2800.HK"
        val measurements = assetClassRepository.findAll().mapNotNull {
            tickerViewRepository.findByIdOrNull(TickerViewId(code, it.id))?.asTicker()?.asTickerMeasurement()
        }
        val measurement = measurements.first()
        measurements.forEach {
            measurement.update(it)
            log.info(measurement.toString())
        }
        assertEquals(ExchangeIds.XHKG, measurement.micCode())
    }
}