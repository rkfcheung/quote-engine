package com.rkfcheung.quoteengine.repository

import com.rkfcheung.quoteengine.AbstractTest
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired

internal class TickerEntityRepositoryTest : AbstractTest() {
    @Autowired
    private lateinit var tickerEntityRepository: TickerEntityRepository

    @Test
    fun testFindById() {
        val ticker = tickerEntityRepository.findById("2800.HK").get()
        log.info(ticker.toString())
        log.info(ticker.asTicker().toString())
    }
}