package com.rkfcheung.quoteengine.repository

import com.rkfcheung.quoteengine.AbstractTest
import com.rkfcheung.quoteengine.model.definition.Resolution
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import java.time.LocalDate

internal class HistPriceRepositoryTest : AbstractTest() {
    @Autowired
    private lateinit var histPriceRepository: HistPriceRepository

    @Test
    fun testFindByCodeAndQuoteDate() {
        val price = histPriceRepository.findByCodeAndQuoteDate("2800.HK", LocalDate.of(2017, 10, 16))
        log.info(price.toString())
        log.info(price.asCandleVPrice(Resolution.DAILY).toString())
    }

    @Test
    fun testFindByCodeAndQuoteDateBetween() {
        val startDate = LocalDate.of(2017, 10, 16)
        val endDate = LocalDate.of(2017, 10, 31)
        val prices = histPriceRepository.findByCodeAndQuoteDateBetween("3968.HK", startDate, endDate)
        prices.forEach { pt ->
            log.info(pt.asLocalDateDoublePoint().toString())
        }

        histPriceRepository.findByQuoteDateBetween(startDate, startDate).take(prices.size).forEach {
            log.info(it.toString())
        }
    }
}