package com.rkfcheung.quoteengine.repository

import com.opengamma.strata.basics.currency.Currency
import com.opengamma.strata.basics.location.Country
import com.rkfcheung.quoteengine.AbstractTest
import com.rkfcheung.quoteengine.model.definition.ExtraTypedConstants
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired

internal class ExchangeCentreRepositoryTest : AbstractTest() {
    @Autowired
    private lateinit var exchangeCentreRepository: ExchangeCentreRepository

    @Test
    fun testFindById() {
        val expected = 1
        val exchangeCentre = exchangeCentreRepository.findById(expected).get()
        log.info(exchangeCentre.toString())
        assertEquals(expected, exchangeCentre.id)
        assertEquals(ExtraTypedConstants.INDEX, exchangeCentre.productType())
    }

    @Test
    fun testFindByCode() {
        val expected = "^GSPC"
        val exchangeCentre = exchangeCentreRepository.findByCode(expected) ?: return
        log.info(exchangeCentre.toString())
        log.info(exchangeCentre.asTicker().toString())
        log.info(exchangeCentre.asTickerEntity(Currency.USD, Country.US.code).toString())
        assertEquals(expected, exchangeCentre.code)
        assertEquals("SPX:IND", exchangeCentre.bloombergCode)
        assertEquals(expected, exchangeCentre.asTicker().yahooCode())
    }

    @Test
    fun testFindAll() {
        val centres = exchangeCentreRepository.findAll()
        centres.groupBy { it.country }.forEach { (type, values) ->
            log.info("$type ${values.size}")
        }
    }
}