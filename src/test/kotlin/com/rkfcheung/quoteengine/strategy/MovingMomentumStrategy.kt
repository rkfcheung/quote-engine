package com.rkfcheung.quoteengine.strategy

import com.opengamma.strata.product.common.ExchangeIds
import com.rkfcheung.quoteengine.AbstractTest
import com.rkfcheung.quoteengine.model.definition.ParameterSource
import com.rkfcheung.quoteengine.model.definition.Resolution
import com.rkfcheung.quoteengine.model.quote.RangeQuoteRequest
import com.rkfcheung.quoteengine.model.ticker.EquityTicker
import com.rkfcheung.quoteengine.rest.QuoteController
import com.rkfcheung.quoteengine.util.UnitUtil.asCandleVPrice
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.ta4j.core.BarSeriesManager
import org.ta4j.core.BaseBarSeries
import org.ta4j.core.BaseStrategy
import org.ta4j.core.Order.OrderType
import org.ta4j.core.analysis.criteria.TotalProfitCriterion
import org.ta4j.core.indicators.EMAIndicator
import org.ta4j.core.indicators.MACDIndicator
import org.ta4j.core.indicators.helpers.ClosePriceIndicator
import org.ta4j.core.trading.rules.CrossedDownIndicatorRule
import org.ta4j.core.trading.rules.CrossedUpIndicatorRule
import org.ta4j.core.trading.rules.OverIndicatorRule
import org.ta4j.core.trading.rules.UnderIndicatorRule
import java.time.ZonedDateTime

internal class MovingMomentumStrategy : AbstractTest() {
    @Autowired
    private lateinit var quoteController: QuoteController

    @Test
    fun testBuildAndRunStrategy() {
        // https://github.com/ta4j/ta4j/blob/master/ta4j-examples/src/main/java/ta4jexamples/strategies/MovingMomentumStrategy.java
        // Getting the bar series
        val years = 20L
        val ticker = EquityTicker(ExchangeIds.XHKG, "1299.HK")
        val endTime = ZonedDateTime.now()
        val startTime = endTime.minusYears(years)
        val request = RangeQuoteRequest(Resolution.DAILY, startTime, endTime, ticker)
        val source = request.parameterSource ?: ParameterSource.YAHOO
        val prices = quoteController.quote(request)
        val series = BaseBarSeries()
        prices.forEach { p ->
            series.addBar(p.asBar())
        }

        // Building the trading strategy
        val closePrice = ClosePriceIndicator(series)

        // The bias is bullish when the shorter-moving average moves above the longer moving average.
        val shortEma = EMAIndicator(closePrice, 9)
        val longEma = EMAIndicator(closePrice, 26)

        val stochasticOscillK = org.ta4j.core.indicators.StochasticOscillatorKIndicator(series, 14)

        val macd = MACDIndicator(closePrice, 9, 26)
        val emaMacd = EMAIndicator(macd, 18)

        // Entry rule
        val entryRule = OverIndicatorRule(shortEma, longEma) // Trend
            .and(CrossedDownIndicatorRule(stochasticOscillK, 20)) // Signal 1
            .and(OverIndicatorRule(macd, emaMacd)) // Signal 2

        // Exit rule
        val exitRule = UnderIndicatorRule(shortEma, longEma) // Trend
            .and(CrossedUpIndicatorRule(stochasticOscillK, 20)) // Signal 1
            .and(UnderIndicatorRule(macd, emaMacd)) // Signal 2

        val strategy = BaseStrategy(entryRule, exitRule)

        // Running the strategy
        val seriesManager = BarSeriesManager(series)
        val tradingRecord = seriesManager.run(strategy, OrderType.BUY, series.numOf(20_000))
        log.info("Number of trades for the strategy: ${tradingRecord.tradeCount}")
        val currency = prices.first().currency
        tradingRecord.trades.forEachIndexed { i, trade ->
            val entryPrice = series.getBar(trade.entry.index).asCandleVPrice(source, ticker, currency)
            val exitPrice = series.getBar(trade.exit.index).asCandleVPrice(source, ticker, currency)
            log.info("[$i] $trade [$entryPrice => $exitPrice]")
        }

        // Analysis
        log.info("Total profit for the strategy: ${TotalProfitCriterion().calculate(series, tradingRecord)}")
    }
}