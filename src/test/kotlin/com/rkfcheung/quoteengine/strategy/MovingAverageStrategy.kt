package com.rkfcheung.quoteengine.strategy

import com.opengamma.strata.basics.currency.Currency
import com.opengamma.strata.product.common.ExchangeIds
import com.rkfcheung.quoteengine.AbstractTest
import com.rkfcheung.quoteengine.model.definition.ParameterSource
import com.rkfcheung.quoteengine.model.definition.Resolution
import com.rkfcheung.quoteengine.model.quote.RangeQuoteRequest
import com.rkfcheung.quoteengine.model.ticker.EquityTicker
import com.rkfcheung.quoteengine.model.ticker.ForexTicker
import com.rkfcheung.quoteengine.rest.QuoteController
import com.rkfcheung.quoteengine.util.UnitUtil.asCandleVPrice
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.ta4j.core.BarSeriesManager
import org.ta4j.core.BaseBarSeries
import org.ta4j.core.BaseStrategy
import org.ta4j.core.Order
import org.ta4j.core.analysis.criteria.*
import org.ta4j.core.cost.LinearTransactionCostModel
import org.ta4j.core.indicators.EMAIndicator
import org.ta4j.core.indicators.helpers.ClosePriceIndicator
import org.ta4j.core.num.Num
import org.ta4j.core.trading.rules.OverIndicatorRule
import org.ta4j.core.trading.rules.UnderIndicatorRule
import java.time.ZonedDateTime
import kotlin.math.ln

internal class MovingAverageStrategy : AbstractTest() {
    @Autowired
    private lateinit var quoteController: QuoteController

    @Test
    fun testStrategy() {
        // Getting the bar series
        val ticker = EquityTicker(ExchangeIds.XHKG, "2800.HK")
//        val ticker = ForexTicker(Currency.EUR, Currency.HKD)
        val years = 10L
        val endTime = ZonedDateTime.now()
        val startTime = endTime.minusYears(years)
        val request = RangeQuoteRequest(Resolution.DAILY, startTime, endTime, ticker)
        val prices = quoteController.quote(request)
        val series = BaseBarSeries()
        prices.forEach { p ->
            series.addBar(p.asBar())
        }

        // Building the trading strategy
        val closePrice = ClosePriceIndicator(series)

        // The bias is bullish when the shorter-moving average moves above the longer moving average.
        // 26 vs 35 for 3988.HK
        // 137 vs 153 for GBP/HKD
        val shortEma = EMAIndicator(closePrice, 26)
        val longEma = EMAIndicator(closePrice, 35)

        // Entry rule
        val entryRule = OverIndicatorRule(shortEma, longEma) // Trend

        // Exit rule
        val exitRule = UnderIndicatorRule(shortEma, longEma) // Trend

        val strategy = BaseStrategy(entryRule, exitRule)

        // Running the strategy
        val costModel = LinearTransactionCostModel(0.00314)
        val seriesManager = BarSeriesManager(series, costModel, costModel)
        val tradingRecord = seriesManager.run(strategy, Order.OrderType.BUY, series.numOf(20_000))
        log.info("Number of trades for the strategy : ${tradingRecord.tradeCount}")
        val source = request.parameterSource ?: ParameterSource.YAHOO
        val currency = prices.first().currency
        tradingRecord.trades.forEachIndexed { i, trade ->
            val entryPrice = series.getBar(trade.entry.index).asCandleVPrice(source, ticker, currency)
            val exitPrice = series.getBar(trade.exit.index).asCandleVPrice(source, ticker, currency)
            log.info("[$i] $trade ${ln(exitPrice.close / entryPrice.close) * 100.0} [${entryPrice.quoteDate} => ${exitPrice.quoteDate}]")
        }

        // Analysis
        log.info("Total Profit for the strategy     : ${TotalProfitCriterion().calculate(series, tradingRecord)}")
        log.info("Maximum Drawdown for the strategy : ${MaximumDrawdownCriterion().calculate(series, tradingRecord)}")
        log.info("Reward Risk Ratio for the strategy: ${RewardRiskRatioCriterion().calculate(series, tradingRecord)}")
        log.info("Buy-And-Hold for the strategy     : ${BuyAndHoldCriterion().calculate(series, tradingRecord)}")
    }

    @Test
    fun testStrategyOptimizeShort() {
        // Getting the bar series
//        val ticker = EquityTicker(ExchangeIds.XHKG, "3988.HK")
        val ticker = ForexTicker(Currency.GBP, Currency.HKD)
        val years = 20L
        val endTime = ZonedDateTime.now().minusYears(10L)
        val startTime = endTime.minusYears(years)
        val request = RangeQuoteRequest(Resolution.DAILY, startTime, endTime, ticker)
        val prices = quoteController.quote(request)
        val series = BaseBarSeries()
        prices.forEach { p ->
            series.addBar(p.asBar())
        }

        // Building the trading strategy
        val closePrice = ClosePriceIndicator(series)
        val result = mutableMapOf<Int, Num>()

        // The bias is bullish when the shorter-moving average moves above the longer moving average.
        var days = 2
        val limit = 153
        while (days < limit) {
            val shortEma = EMAIndicator(closePrice, days)
            val longEma = EMAIndicator(closePrice, limit)

            // Entry rule
            val entryRule = OverIndicatorRule(shortEma, longEma) // Trend

            // Exit rule
            val exitRule = UnderIndicatorRule(shortEma, longEma) // Trend

            val strategy = BaseStrategy(entryRule, exitRule)

            // Running the strategy
            val costModel = LinearTransactionCostModel(0.00314)
            val seriesManager = BarSeriesManager(series, costModel, costModel)
            val tradingRecord = seriesManager.run(strategy, Order.OrderType.BUY, series.numOf(20_000))
            log.info("Number of trades for the strategy : ${tradingRecord.tradeCount}")

            // Analysis
            val criterion = RewardRiskRatioCriterion().calculate(series, tradingRecord)
            log.info("Total Profit for the strategy     : ${TotalProfitCriterion().calculate(series, tradingRecord)}")
            log.info(
                "Maximum Drawdown for the strategy : ${
                    MaximumDrawdownCriterion().calculate(
                        series,
                        tradingRecord
                    )
                }"
            )
            log.info("Reward Risk Ratio for the strategy: $criterion")
            result[days] = criterion

            days += 1
        }

        var maxRatio = series.numOf(0.0)
        var optimized = 0
        result.forEach { (days, ratio) ->
            log.info("Reward Risk Ratio for the strategy [$days]: $ratio")
            if (ratio > maxRatio) {
                maxRatio = ratio
                optimized = days
            }
        }
        log.info("Optimized: $optimized => $maxRatio")
    }

    @Test
    fun testStrategyOptimizeLong() {
        // Getting the bar series
//        val ticker = EquityTicker(ExchangeIds.XHKG, "3988.HK")

        val ticker = ForexTicker(Currency.GBP, Currency.HKD)
        val years = 20L
        val endTime = ZonedDateTime.now().minusYears(10L)
        val startTime = endTime.minusYears(years)
        val request = RangeQuoteRequest(Resolution.DAILY, startTime, endTime, ticker)
        val prices = quoteController.quote(request)
        val series = BaseBarSeries()
        prices.forEach { p ->
            series.addBar(p.asBar())
        }

        // Building the trading strategy
        val closePrice = ClosePriceIndicator(series)
        val result = mutableMapOf<Int, Num>()

        // The bias is bullish when the shorter-moving average moves above the longer moving average.
        var days = 138
        val limit = 512
        while (days <= limit) {
            val shortEma = EMAIndicator(closePrice, 137)
            val longEma = EMAIndicator(closePrice, days)

            // Entry rule
            val entryRule = OverIndicatorRule(shortEma, longEma) // Trend

            // Exit rule
            val exitRule = UnderIndicatorRule(shortEma, longEma) // Trend

            val strategy = BaseStrategy(entryRule, exitRule)

            // Running the strategy
            val costModel = LinearTransactionCostModel(0.00314)
            val seriesManager = BarSeriesManager(series, costModel, costModel)
            val tradingRecord = seriesManager.run(strategy, Order.OrderType.BUY, series.numOf(20_000))
            log.info("Number of trades for the strategy : ${tradingRecord.tradeCount}")

            // Analysis
            val criterion = RewardRiskRatioCriterion().calculate(series, tradingRecord)
            log.info("Total Profit for the strategy     : ${TotalProfitCriterion().calculate(series, tradingRecord)}")
            log.info(
                "Maximum Drawdown for the strategy : ${
                    MaximumDrawdownCriterion().calculate(
                        series,
                        tradingRecord
                    )
                }"
            )
            log.info("Reward Risk Ratio for the strategy: $criterion")
            result[days] = criterion

            days += 1
        }

        var maxRatio = series.numOf(0.0)
        var optimized = 0
        result.forEach { (days, ratio) ->
            log.info("Reward Risk Ratio for the strategy [$days]: $ratio")
            if (ratio > maxRatio) {
                maxRatio = ratio
                optimized = days
            }
        }
        log.info("Optimized: $optimized => $maxRatio")
    }
}