package com.rkfcheung.quoteengine.connector

import com.opengamma.strata.basics.currency.Currency
import com.opengamma.strata.product.common.ExchangeIds
import com.rkfcheung.quoteengine.AbstractTest
import com.rkfcheung.quoteengine.model.definition.Resolution
import com.rkfcheung.quoteengine.model.quote.YahooQuoteRequest
import com.rkfcheung.quoteengine.model.ticker.EquityTicker
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import java.time.ZonedDateTime

internal class YahooEquityConnectorTest : AbstractTest() {
    @Autowired
    private lateinit var yahooEquityConnector: YahooEquityConnector

    @Test
    fun testQuote() {
        val hkTrackerFund = EquityTicker(ExchangeIds.XHKG, "02800")
        val today = ZonedDateTime.now()
        val startTime = today.minusMonths(1L)
        val request = YahooQuoteRequest(Resolution.DAILY, startTime, today, hkTrackerFund)
        val prices = yahooEquityConnector.quote(request)
        assertTrue(prices.isNotEmpty())
        prices.forEach {
            log.info(it.toString())
            assertEquals(Resolution.DAILY, it.resolution)
            assertEquals(Currency.HKD, it.currency)
        }
    }
}