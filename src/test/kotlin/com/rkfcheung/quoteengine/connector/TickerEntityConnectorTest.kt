package com.rkfcheung.quoteengine.connector

import com.opengamma.strata.basics.StandardId
import com.opengamma.strata.basics.StandardSchemes
import com.opengamma.strata.product.ProductType
import com.opengamma.strata.product.common.ExchangeIds
import com.rkfcheung.quoteengine.AbstractTest
import com.rkfcheung.quoteengine.model.definition.BloombergSecurityType
import com.rkfcheung.quoteengine.model.definition.ExtraTypedConstants
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired

internal class TickerEntityConnectorTest : AbstractTest() {
    @Autowired
    private lateinit var tickerEntityConnector: TickerEntityConnector

    @Test
    fun testGetAssetClasses() {
        tickerEntityConnector.getAssetClasses().forEach {
            when (it.id) {
                1 -> assertEquals(ProductType.SECURITY, it.asProductType())
                2 -> assertEquals(ProductType.ETD_OPTION, it.asProductType())
                3 -> assertEquals(ProductType.FX_SINGLE, it.asProductType())
                4 -> assertEquals(ExtraTypedConstants.PENSION_FUND, it.asProductType())
                5 -> assertEquals(ExtraTypedConstants.INSURANCE, it.asProductType())
                6 -> assertEquals(ExtraTypedConstants.FUND, it.asProductType())
                7 -> assertEquals(ExtraTypedConstants.INDEX, it.asProductType())
                8 -> assertEquals(ExtraTypedConstants.SELF_FUND, it.asProductType())
                9 -> assertEquals(ExtraTypedConstants.SELF_MARKET_INDEX, it.asProductType())
                18 -> assertEquals(ExtraTypedConstants.SELF_ECONOMIC_INDEX, it.asProductType())
                else -> assertEquals(ProductType.of(it.name.replace(" ", "")), it.asProductType())
            }
        }
    }

    @Test
    fun testGetTicker() {
        val code = "823:HK"
        val ticker = tickerEntityConnector.getTicker(code)
        log.info(ticker?.toString())
        assertEquals(BloombergSecurityType.REIT.asProductType(), ticker?.type)
        assertEquals(ExchangeIds.XHKG, ticker?.micCode())
        assertEquals(StandardId.of(StandardSchemes.BBG_SCHEME, code), ticker?.findId(StandardSchemes.BBG_SCHEME))
        assertTrue(ticker?.ids()?.find { it.scheme == StandardSchemes.FIGI_SCHEME } != null)
    }

    @Test
    fun testGetTickerEntity() {
        val code = "541.HK"
        val ticker = tickerEntityConnector.get(code)
        log.info(ticker.toString())
        assertEquals("0541.HK", ticker.code)
    }
}