package com.rkfcheung.quoteengine.connector

import com.opengamma.strata.basics.currency.Currency
import com.rkfcheung.quoteengine.AbstractTest
import com.rkfcheung.quoteengine.model.definition.Resolution
import com.rkfcheung.quoteengine.model.quote.RangeQuoteRequest
import com.rkfcheung.quoteengine.model.ticker.ForexTicker
import com.rkfcheung.quoteengine.util.DateTimeUtil.asZonedDateTime
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import java.time.LocalDate

internal class HistPriceConnectorTest: AbstractTest() {
    @Autowired
    private lateinit var histPriceConnector: HistPriceConnector

    @Test
    fun testQuote() {
        val startDate = LocalDate.of(2017, 10, 16).asZonedDateTime()
        val endDate = startDate.plusWeeks(1L)
        val forexTicker = ForexTicker(Currency.GBP, Currency.HKD)
        val request = RangeQuoteRequest(Resolution.DAILY, startDate, endDate, forexTicker)
        val prices = histPriceConnector.quote(request)
        prices.forEach { p ->
            log.info("${p.asCandlePrice()}")
            Assertions.assertEquals(Resolution.DAILY, p.resolution)
            Assertions.assertEquals(Currency.HKD, p.currency)
        }
    }
}