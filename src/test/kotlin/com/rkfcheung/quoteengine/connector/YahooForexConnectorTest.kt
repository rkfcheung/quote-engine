package com.rkfcheung.quoteengine.connector

import com.opengamma.strata.basics.currency.Currency
import com.rkfcheung.quoteengine.AbstractTest
import com.rkfcheung.quoteengine.model.ticker.ForexTicker
import com.rkfcheung.quoteengine.util.UnitUtil.asForexTicker
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired

internal class YahooForexConnectorTest : AbstractTest() {
    @Autowired
    private lateinit var yahooForexConnector: YahooForexConnector

    @Test
    fun testQuote() {
        val gbpHKD = ForexTicker(Currency.GBP, Currency.HKD)
        val gbpHKDPrice = yahooForexConnector.quote(gbpHKD)
        assertTrue(gbpHKDPrice.value > 0)
        log.info("[${gbpHKD.pair}] $gbpHKDPrice")

        val gbpUSD = ForexTicker(Currency.GBP, Currency.USD)
        val gbpUSDPrice = yahooForexConnector.quote(gbpUSD)
        assertTrue(gbpUSDPrice.value > 0)
        log.info("[${gbpUSD.pair}] $gbpUSDPrice")

        val eurUSD = ForexTicker("EUR", "USD")
        val eurUSDPrice = yahooForexConnector.quote(eurUSD)
        assertTrue(eurUSDPrice.value > 0)
        log.info("[${eurUSD.pair}] $eurUSDPrice")

        val usdEur = eurUSD.inverse()
        log.info("${usdEur.pair} => ${usdEur.pair.asForexTicker()}")
        val usdEurPrice = yahooForexConnector.quote(usdEur.pair.asForexTicker())
        assertTrue(usdEurPrice.value > 0)
        log.info("[${usdEur.pair}] $usdEurPrice")
        log.info("[${usdEur.pair}] ${usdEurPrice.asFxRate()}")
        log.info("[${usdEur.pair}] ${usdEurPrice.asCandlePrice()}")
    }
}