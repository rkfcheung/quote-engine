package com.rkfcheung.quoteengine.connector

import com.opengamma.strata.basics.currency.Currency
import com.opengamma.strata.product.common.ExchangeIds
import com.rkfcheung.quoteengine.AbstractTest
import com.rkfcheung.quoteengine.model.definition.Resolution
import com.rkfcheung.quoteengine.model.quote.QuandlQuoteRequest
import com.rkfcheung.quoteengine.model.ticker.EquityTicker
import com.rkfcheung.quoteengine.util.MarketMapper
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import java.time.ZonedDateTime

internal class QuandlEquityConnectorTest : AbstractTest() {
    @Autowired
    private lateinit var quandlEquityConnector: QuandlEquityConnector

    @Test
    fun testQuote() {
        val hkTrackerFund = EquityTicker(ExchangeIds.XHKG, "02800")
        assertEquals("${MarketMapper.HKEX}/${hkTrackerFund.code}", hkTrackerFund.quandlCode())
        log.info("$hkTrackerFund => ${hkTrackerFund.quandlCode()}")
        val today = ZonedDateTime.now()
        val startTime = today.minusMonths(1L)
        val request = QuandlQuoteRequest(Resolution.DAILY, startTime, today, hkTrackerFund)
        val prices = quandlEquityConnector.quote(request)
        assertTrue(prices.isNotEmpty())
        prices.forEach {
            log.info("$it ${it.quasiTurnover()}")
            assertEquals(Resolution.DAILY, it.resolution)
            assertEquals(Currency.HKD, it.currency)
        }
    }
}