package com.rkfcheung.quoteengine.client

import com.opengamma.strata.basics.currency.Currency
import com.opengamma.strata.data.ObservableSource
import com.opengamma.strata.product.ProductType
import com.rkfcheung.quoteengine.AbstractTest
import com.rkfcheung.quoteengine.model.definition.PriceAttribute
import com.rkfcheung.quoteengine.model.definition.Resolution
import com.rkfcheung.quoteengine.model.quote.RangeQuoteRequest
import com.rkfcheung.quoteengine.model.ticker.ForexTicker
import com.rkfcheung.quoteengine.model.ticker.Ticker
import com.rkfcheung.quoteengine.util.MarketMapper
import com.rkfcheung.quoteengine.util.YahooUtil
import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import java.time.ZonedDateTime

@InternalCoroutinesApi
internal class FinnhubClientTest : AbstractTest() {
    @Autowired
    private lateinit var finnhubClient: FinnhubClient

    private val testExchange = MarketMapper.HK.code

    @Test
    fun testGetStockCandles() = runBlocking {
        val candles = finnhubClient.getStockCandles("AAPL", "D", 1590988249, 1591852249)
        assertEquals("ok", candles.s)
    }

    @Test
    fun testGetStockCandles2() = runBlocking {
        val endTime = ZonedDateTime.now()
        val request = RangeQuoteRequest(
            Resolution.DAILY,
            endTime.minusMonths(1L),
            endTime,
            Ticker(ProductType.SECURITY, "AAPL")
        )
        val candles = finnhubClient.quote(request)
        candles.forEach {
            log.info("$it")
            assertTrue(it.hasAttribute(PriceAttribute.VOLUME))
            assertEquals(Currency.USD, it.currency)
        }
    }

    @Test
    fun testGetStockSymbols() = runBlocking {
        val testSymbols = setOf("4.HK", "52.HK", "823.HK")
        val symbols = finnhubClient.getStockSymbols(testExchange).filter {
            testSymbols.contains(it.symbol) || it.symbol.length > 8
        }.toList()
        assertTrue(symbols.isNotEmpty())
        symbols.forEach {
            val ticker = it.asTicker()
            val tickerEntity = it.asTickerEntity()
            log.info("$it => $ticker")
            assertEquals(it.symbol.replace(".", ":"), it.bloombergCode())
            when (it.code) {
                "4.HK" -> {
                    assertEquals("0004.HK", it.code)
                    assertEquals("BBG000BD5H69", it.figiCode())
                }
                "52.HK" -> {
                    assertEquals("0052.HK", it.code)
                }
                "823.HK" -> {
                    assertEquals("0823.HK", it.code)
                    assertEquals(Currency.HKD, it.currency())
                }
            }
            assertTrue(MarketMapper.isHKG(it.micCode()))
            assertEquals(testExchange, tickerEntity.market)
            assertEquals(0, tickerEntity.lotSize)
        }

        testSymbols.forEach { code ->
            val ticker = finnhubClient.searchTicker(code)
            assertTrue(ticker != null)
            assertEquals(YahooUtil.toBloombergCode(code), ticker?.bloombergCode())
        }
    }

    @Test
    fun testSearchSymbol() = runBlocking {
        val result = finnhubClient.searchSymbol("apple")
        log.info(result.toString())
        assertTrue(result.count > 0)
    }

    @Test
    fun testGetForexChanges() = runBlocking {
        val exchanges = finnhubClient.getForexExchanges()
        exchanges.forEach {
            log.info(it)
        }
    }

    @Test
    fun testGetForexCandles() = runBlocking {
        val candles = finnhubClient.getForexCandles("OANDA:EUR_USD", "D", 1590988249, 1591852249)
        assertEquals("ok", candles.s)
    }

    @Test
    fun testGetForexCandles2() = runBlocking {
        val endTime = ZonedDateTime.now()
        val ticker = ForexTicker(Currency.GBP, Currency.HKD)
        val request = RangeQuoteRequest(
            Resolution.DAILY,
            endTime.minusMonths(1L),
            endTime,
            ticker,
            ObservableSource.of("OANDA")
        )
        val candles = finnhubClient.quote(request)
        candles.forEach {
            log.info("$it")
            assertTrue(it.hasAttribute(PriceAttribute.VOLUME))
            assertEquals(ticker.currency(), it.currency)
        }
    }

    @Test
    fun testGetForexCandles3() = runBlocking {
        val endTime = ZonedDateTime.now()
        val request = RangeQuoteRequest(
            Resolution.DAILY,
            endTime.minusMonths(1L),
            endTime,
            ForexTicker(Currency.EUR, Currency.USD)
        )
        val candles = finnhubClient.quote(request)
        candles.forEach {
            log.info("$it")
            assertTrue(it.hasAttribute(PriceAttribute.VOLUME))
            assertEquals(Currency.USD, it.currency)
        }
    }

    @Test
    fun testGetForexRange() = runBlocking {
        val endTime = ZonedDateTime.now()
        val request = RangeQuoteRequest(
            Resolution.DAILY,
            endTime.minusYears(2L),
            endTime,
            ForexTicker(Currency.EUR, Currency.USD)
        )
        val candles = finnhubClient.quote(request)
        val first = candles.firstOrNull()
        val last = candles.lastOrNull()
        assertTrue(first != null)
        log.info("$first $last => ${candles.size}")
    }
}