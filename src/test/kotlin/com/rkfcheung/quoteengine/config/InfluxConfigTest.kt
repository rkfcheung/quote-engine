package com.rkfcheung.quoteengine.config

import com.opengamma.strata.data.ObservableSource
import com.opengamma.strata.product.ProductType
import com.rkfcheung.quoteengine.AbstractTest
import com.rkfcheung.quoteengine.model.definition.MeasurementBasics
import com.rkfcheung.quoteengine.model.definition.Resolution
import com.rkfcheung.quoteengine.model.price.measurement.TestPriceMeasurement
import com.rkfcheung.quoteengine.model.price.measurement.TestPriceUtil
import com.rkfcheung.quoteengine.model.quote.RangeQuoteRequest
import com.rkfcheung.quoteengine.model.ticker.Ticker
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.consumeAsFlow
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.take
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import java.time.ZonedDateTime
import kotlin.random.Random

internal class InfluxConfigTest : AbstractTest() {
    @Autowired
    private lateinit var influxConfig: InfluxConfig

    @Test
    fun testInfluxClient() = runBlocking {
        val client = influxConfig.influxClient()
        val code = TestPriceUtil.TEST_CODE
        repeat(16) {
            val timestamp = ZonedDateTime.now().minusMinutes(Random.nextLong(0L, 8L))
            val price = TestPriceMeasurement(code, timestamp.toInstant(), Random.nextDouble())
            client.writeApi.writeMeasurement(MeasurementBasics.WRITE_PRECISION, price)
        }
        client.writeApi.flush()

        val results = client.queryApi.query(
            """
from(bucket: "${influxConfig.bucket()}")
  |> range(start: -1h)
  |> filter(fn: (r) => r["_measurement"] == "${TestPriceUtil.TEST_MEASUREMENT}")
  |> filter(fn: (r) => r["${MeasurementBasics.CODE}"] == "$code")  
  |> filter(fn: (r) => r["_field"] == "${MeasurementBasics.VALUE}") 
            """.trimIndent()
        )
        results.map { it.records }.flatten().take(16).forEach { r ->
            log.info("${r.values}")
        }

        client.close()
    }

    @Test
    fun testInfluxQueryClient() = runBlocking {
        val client = influxConfig.influxQueryClient()
        val priceClient = influxConfig.influxPriceClient()
        val now = ZonedDateTime.now()
        val ticker = Ticker(ProductType.OTHER, TestPriceUtil.TEST_CODE)
        val request = RangeQuoteRequest(Resolution.NONE, now.minusHours(1L), now, ticker, ObservableSource.NONE)
        val query =
            priceClient.prepareQuery(request, TestPriceUtil.TEST_MEASUREMENT, listOf("bool"), request.parameterSource)
        log.info(query)

        val results = client.getQueryKotlinApi().query(query)
        results.consumeAsFlow().filter { it.field == MeasurementBasics.VALUE }.take(16).collect { r ->
            assertEquals(TestPriceUtil.TEST_MEASUREMENT, r.measurement)
            log.info("${r.measurement} ${r.field} ${r.value}")
        }

        client.close()
    }
}