package com.rkfcheung.quoteengine.config

import com.jimmoores.quandl.classic.ClassicQuandlSession
import com.opengamma.strata.basics.currency.Currency
import com.opengamma.strata.product.ProductType
import com.opengamma.strata.product.common.ExchangeIds
import com.rkfcheung.quoteengine.AbstractTest
import com.rkfcheung.quoteengine.model.definition.Resolution
import com.rkfcheung.quoteengine.model.quote.QuandlQuoteRequest
import com.rkfcheung.quoteengine.model.ticker.EquityTicker
import com.rkfcheung.quoteengine.model.ticker.Ticker
import com.rkfcheung.quoteengine.util.MarketMapper
import com.rkfcheung.quoteengine.util.QuandlUtil
import com.rkfcheung.quoteengine.util.QuandlUtil.asCandleVPrice
import com.rkfcheung.quoteengine.util.QuandlUtil.asPrices
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import java.time.ZonedDateTime

internal class QuandlConfigTest : AbstractTest() {
    @Autowired
    private lateinit var quandlSession: ClassicQuandlSession

    @Test
    fun testQuandlSession() {
        val hkTrackerFund = EquityTicker(ExchangeIds.XHKG, "02800")
        assertEquals("${MarketMapper.HKEX}/02800", hkTrackerFund.quandlCode())
        val today = ZonedDateTime.now()
        val startTime = today.minusMonths(1L)
        val request = QuandlQuoteRequest(Resolution.DAILY, startTime, today, hkTrackerFund)
        val resultHKTrackFund = quandlSession.getDataSet(request.asDataSetRequest())
        assertNotNull(resultHKTrackFund)
        log.info(resultHKTrackFund.headerDefinition.toString())
        resultHKTrackFund.take(8).forEach { r ->
            log.info(
                r.asCandleVPrice(
                    resultHKTrackFund.headerDefinition,
                    Resolution.DAILY,
                    hkTrackerFund,
                    Currency.HKD
                ).toString()
            )
        }
    }

    @Test
    fun testGetApple() {
        val apple = QuandlUtil.toCode("${MarketMapper.WIKI}/AAPL") ?: return
        quandlSession.getDataSet(apple).asPrices(Resolution.DAILY, Ticker(ProductType.SECURITY, apple), Currency.USD)
            .take(8).forEach {
                log.info(it.toString())
            }
    }
}