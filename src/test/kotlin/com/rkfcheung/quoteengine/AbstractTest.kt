package com.rkfcheung.quoteengine

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ActiveProfiles

@SpringBootTest
@ActiveProfiles("local")
abstract class AbstractTest {
    protected val log: Logger = LoggerFactory.getLogger(this.javaClass)
}