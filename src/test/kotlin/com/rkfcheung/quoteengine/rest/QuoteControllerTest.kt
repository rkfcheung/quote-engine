package com.rkfcheung.quoteengine.rest

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.opengamma.strata.data.ObservableSource
import com.rkfcheung.quoteengine.AbstractTest
import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*
import org.springframework.beans.factory.annotation.Autowired
import java.time.LocalDate

internal class QuoteControllerTest : AbstractTest() {
    @Autowired
    private lateinit var quoteController: QuoteController

    private val objectMapper = jacksonObjectMapper()

    @Test
    fun testQuote() {
        val prices = quoteController.quote("2800.HK", 1L)
        prices.takeLast(8).forEach {
            assertTrue(it.quasiTurnover() > 0)
            assertNotEquals(it.volume, it.open)
        }
    }

    @Test
    fun testForexQuote() {
        val prices = quoteController.quote(
            "EURUSD=X",
            startDate = LocalDate.of(2018, 8, 30),
            endDate = LocalDate.of(2019, 8, 19)
        )
        prices.take(8).forEach {
            log.info(it.toString())
        }
        prices.takeLast(8).forEach {
            log.info(it.toString())
        }
    }

    @Test
    fun testForexQuote2() {
        val prices = quoteController.quote("GBPHKD=X", 20L, source = ObservableSource.of("OANDA"))
        prices.take(8).forEach {
            log.info(it.toString())
        }
        prices.takeLast(8).forEach {
            log.info(objectMapper.writeValueAsString(it))
        }
    }
}