package com.rkfcheung.quoteengine.repository

import com.rkfcheung.quoteengine.model.entity.BloombergTicker
import org.springframework.data.repository.CrudRepository

interface BloombergTickerRepository: CrudRepository<BloombergTicker, String> {
    fun findByCode(code: String): BloombergTicker?
}