package com.rkfcheung.quoteengine.repository

import com.rkfcheung.quoteengine.model.entity.ExchangeCentre
import org.springframework.data.repository.CrudRepository

interface ExchangeCentreRepository: CrudRepository<ExchangeCentre, Int> {
    fun findByCode(code: String): ExchangeCentre?
}