package com.rkfcheung.quoteengine.repository

import com.rkfcheung.quoteengine.model.entity.HistPrice
import com.rkfcheung.quoteengine.model.entity.HistPriceId
import org.springframework.data.repository.CrudRepository
import java.time.LocalDate

interface HistPriceRepository : CrudRepository<HistPrice, HistPriceId> {
    fun findByCodeAndQuoteDate(code: String, quoteDate: LocalDate): HistPrice

    fun findByCodeAndQuoteDateBetween(code: String, startDate: LocalDate, endDate: LocalDate): List<HistPrice>

    fun findByQuoteDateBetween(startDate: LocalDate, endDate: LocalDate): List<HistPrice>
}