package com.rkfcheung.quoteengine.repository

import com.rkfcheung.quoteengine.model.entity.AssetClass
import org.springframework.data.repository.CrudRepository

interface AssetClassRepository: CrudRepository<AssetClass, Int>
