package com.rkfcheung.quoteengine.repository

import com.rkfcheung.quoteengine.model.entity.HkExSecurity
import org.springframework.data.repository.CrudRepository

interface HkExSecurityRepository : CrudRepository<HkExSecurity, String>
