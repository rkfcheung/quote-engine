package com.rkfcheung.quoteengine.repository

import com.rkfcheung.quoteengine.model.entity.TickerView
import com.rkfcheung.quoteengine.model.entity.TickerViewId
import org.springframework.data.repository.CrudRepository

interface TickerViewRepository : CrudRepository<TickerView, TickerViewId>
