package com.rkfcheung.quoteengine.repository

import com.rkfcheung.quoteengine.model.entity.TickerEntity
import org.springframework.data.repository.CrudRepository

interface TickerEntityRepository: CrudRepository<TickerEntity, String>
