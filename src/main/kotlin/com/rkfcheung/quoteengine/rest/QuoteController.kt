package com.rkfcheung.quoteengine.rest

import com.opengamma.strata.data.ObservableSource
import com.rkfcheung.quoteengine.config.RestConstants
import com.rkfcheung.quoteengine.model.definition.Resolution
import com.rkfcheung.quoteengine.model.price.CandleVPrice
import com.rkfcheung.quoteengine.model.quote.RangeQuoteRequest
import com.rkfcheung.quoteengine.service.QuoteService
import com.rkfcheung.quoteengine.service.TickerService
import com.rkfcheung.quoteengine.util.DateTimeUtil.asZonedDateTime
import com.rkfcheung.quoteengine.util.TickerUtil
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.cache.annotation.Cacheable
import org.springframework.format.annotation.DateTimeFormat
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import java.time.LocalDate
import java.time.ZonedDateTime

@RestController
class QuoteController {
    @Autowired
    private lateinit var quoteService: QuoteService

    @Autowired
    private lateinit var tickerService: TickerService

    private val log: Logger = LoggerFactory.getLogger(this.javaClass)

    @GetMapping("${RestConstants.API_QUOTE_BASE}/{code}")
    @Cacheable
    fun quote(
        @PathVariable code: String,
        @RequestParam years: Long? = null,
        @RequestParam resolution: Resolution? = null,
        @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) startDate: LocalDate? = null,
        @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) endDate: LocalDate? = null,
        @RequestParam source: ObservableSource? = null
    ): List<CandleVPrice> {
        val endTime = endDate?.asZonedDateTime() ?: ZonedDateTime.now()
        val startTime = startDate?.asZonedDateTime() ?: endTime.minusYears(years ?: 5L)
        val ticker = tickerService.search(code) ?: TickerUtil.asTicker(code)
        val request = RangeQuoteRequest(resolution ?: Resolution.DAILY, startTime, endTime, ticker, source)
        return quote(request)
    }

    fun quote(request: RangeQuoteRequest): List<CandleVPrice> {
        log.info("Requesting $request ...")
        return quoteService.quote(request)
    }
}