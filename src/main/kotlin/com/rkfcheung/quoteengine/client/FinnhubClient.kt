package com.rkfcheung.quoteengine.client

import com.finnhub.api.models.StockCandles
import com.opengamma.strata.basics.currency.Currency
import com.opengamma.strata.basics.location.Country
import com.opengamma.strata.data.ObservableSource
import com.opengamma.strata.product.ProductType
import com.rkfcheung.quoteengine.config.FinnhubProperties
import com.rkfcheung.quoteengine.connector.RangeQuoteConnector
import com.rkfcheung.quoteengine.model.definition.ParameterSource
import com.rkfcheung.quoteengine.model.definition.Resolution
import com.rkfcheung.quoteengine.model.entity.finnhub.StockSymbol
import com.rkfcheung.quoteengine.model.entity.finnhub.StockSymbolResult
import com.rkfcheung.quoteengine.model.price.CandleVPrice
import com.rkfcheung.quoteengine.model.quote.RangeQuoteRequest
import com.rkfcheung.quoteengine.model.ticker.ForexTicker
import com.rkfcheung.quoteengine.model.ticker.Ticker
import com.rkfcheung.quoteengine.util.*
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.firstOrNull
import org.springframework.cache.annotation.Cacheable
import org.springframework.web.reactive.function.client.WebClient
import org.springframework.web.reactive.function.client.awaitBody
import org.springframework.web.reactive.function.client.bodyToFlow
import java.time.Instant
import java.time.LocalDate
import java.time.ZoneId
import java.time.ZonedDateTime

class FinnhubClient(
    properties: FinnhubProperties
) : RangeQuoteConnector<RangeQuoteRequest, CandleVPrice>() {
    override val parameterSource: ObservableSource = ParameterSource.FINNHUB
    override val productType: ProductType = ProductType.SECURITY
    private val client = WebClient.builder().baseUrl(properties.url).build()
    private val token = properties.token
    private val regex = Regex("[^A-Za-z0-9]")
    private val since = ZonedDateTime.now().minusYears(20L)

    @Cacheable
    override fun quote(request: RangeQuoteRequest): List<CandleVPrice> = runBlocking {
        val code = request.parameterCode
        val ticker = if (code.currency() == null || code.currency() == Currency.XXX) {
            val yhCode = YahooUtil.toCode(code.yahooCode(), code.micCode())
            searchTicker(code.finnhubCode() ?: yhCode) ?: TickerUtil.asTicker(yhCode, productType)
        } else {
            code
        }
        return@runBlocking withContext(Dispatchers.IO) {
            val prices = mutableListOf<CandleVPrice>()
            var start = DateTimeUtil.max(request.startTime, since)
            val stop = DateTimeUtil.max(request.endTime, since)
            while (start.isBefore(stop)) {
                val endTime = DateTimeUtil.min(start.plusYears(1L), stop)
                prices += try {
                    getCandles(ticker, request.resolution, start, endTime, request.parameterSource)
                } catch (e: Exception) {
                    log.warn("Failed to get ${ticker.description()} on $parameterSource, $e!")
                    emptyList()
                }
                start = endTime
            }

            prices
        }
    }

    // https://finnhub.io/docs/api/forex-exchanges
    @Cacheable
    suspend fun getForexExchanges(): List<String> = client.get()
        .uri("/forex/exchange?token={token}", token)
        .retrieve()
        .awaitBody()

    // https://finnhub.io/docs/api/forex-candles
    @Cacheable
    suspend fun getForexCandles(
        symbol: String, resolution: String, from: Long, to: Long
    ): StockCandles = client.get()
        .uri(
            "/forex/candle?symbol={symbol}&resolution={resolution}&from={from}&to={to}&token={token}",
            symbol,
            resolution,
            from,
            to,
            token
        )
        .retrieve()
        .awaitBody()

    // https://finnhub.io/docs/api/stock-symbols
    @Cacheable
    fun getStockSymbols(exchange: String): Flow<StockSymbol> = client.get()
        .uri("/stock/symbol?exchange={exchange}&token={token}", exchange, token)
        .retrieve()
        .bodyToFlow()

    // https://finnhub.io/docs/api/stock-candles
    @Cacheable
    suspend fun getStockCandles(
        symbol: String, resolution: String, from: Long, to: Long
    ): StockCandles = client.get()
        .uri(
            "/stock/candle?symbol={symbol}&resolution={resolution}&from={from}&to={to}&token={token}",
            symbol,
            resolution,
            from,
            to,
            token
        )
        .retrieve()
        .awaitBody()

    // https://finnhub.io/docs/api/symbol-search
    @Cacheable
    suspend fun searchSymbol(q: String): StockSymbolResult = client.get()
        .uri("/search?q={q}&token={token}", q, token)
        .retrieve()
        .awaitBody()

    @Cacheable
    suspend fun searchTicker(code: String): Ticker? {
        val pair = YahooUtil.toCodePair(code)
        val market = pair?.second ?: Country.US.code
        val bbCode = YahooUtil.toBloombergCode(code) ?: code
        val yhCode = toYahooCode(code, market)
        if (market.length == 2) {
            log.info("Searching Ticker $code on ${ParameterSource.FINNHUB} ...")
            return getStockSymbols(market).firstOrNull {
                setOf(it.symbol, it.code, it.bloombergCode() == code).contains(code) ||
                        it.code == yhCode ||
                        it.bloombergCode() == bbCode
            }?.asTicker()?.apply {
                if (MarketMapper.isUSA(market)) {
                    addLotSize(1)
                }
            }
        }
        return null
    }

    @Cacheable
    private suspend fun getCandles(
        ticker: Ticker,
        resolution: Resolution,
        startTime: ZonedDateTime,
        endTime: ZonedDateTime,
        source: ObservableSource? = null
    ): List<CandleVPrice> {
        val resolutionCode = when (resolution) {
            Resolution.WEEKLY -> "W"
            Resolution.MONTHLY -> "M"
            else -> "D"
        }
        val code: String
        val startDate = startTime.toLocalDate()
        val endDate = endTime.toLocalDate()
        val candles: StockCandles
        val candleSource: ObservableSource
        if (ticker is ForexTicker) {
            val defaultExchange = getForexExchanges().first()
            val forexExchange =
                (getForexExchanges().firstOrNull { asObservableSource(it) == source } ?: defaultExchange).toUpperCase()
            candleSource = asObservableSource(forexExchange)
            code = "$forexExchange:${ticker.base}_${ticker.counter}"
            candles = getForexCandles(
                code,
                resolutionCode,
                startTime.toInstant().epochSecond,
                endTime.toInstant().epochSecond
            )
        } else {
            code = toFinnhubStockCode(ticker)
            candleSource = parameterSource
            candles = getStockCandles(
                code,
                resolutionCode,
                startTime.toInstant().epochSecond,
                endTime.toInstant().epochSecond
            )
        }
        log.info("Candles($code, $startTime, $endTime) => ${candles.s}, ${candles.c?.size}")
        return toCandlePrices(ticker, resolution, candles, candleSource).filter {
            (it.quoteDate.isEqual(startDate) || it.quoteDate.isAfter(startDate)) &&
                    (it.quoteDate.isEqual(endDate) || it.quoteDate.isBefore(endDate))
        }
    }

    private fun toCandlePrices(
        ticker: Ticker,
        resolution: Resolution,
        candles: StockCandles,
        candleSource: ObservableSource
    ): List<CandleVPrice> {
        val timestamps = candles.t?.map { Instant.ofEpochSecond(it) } ?: emptyList()
        val closes = candles.c ?: emptyList()
        if (candles.s != "ok" || timestamps.isEmpty() || closes.isEmpty()) {
            return emptyList()
        }

        val opens = candles.o ?: closes
        val highs = candles.h ?: closes
        val lows = candles.l ?: closes
        val volumes = candles.v

        val prices = mutableListOf<CandleVPrice>()
        timestamps.forEachIndexed { i, ts ->
            val volume = volumes?.get(i)?.let { UnitUtil.toDouble(it.toDouble()) } ?: 0.0
            prices += CandleVPrice(
                resolution,
                candleSource,
                LocalDate.ofInstant(ts, ZoneId.systemDefault()),
                ticker,
                opens[i].toDouble(),
                highs[i].toDouble(),
                lows[i].toDouble(),
                closes[i].toDouble(),
                volume,
                ticker.currency() ?: Currency.XXX
            )
        }
        return prices
    }

    private fun toFinnhubStockCode(ticker: Ticker) = ticker.finnhubCode()
        ?: (ticker.bloombergCode()?.replace(":", ".") ?: ticker.yahooCode()).trimStart('0')

    private fun toYahooCode(code: String, market: String) =
        YahooUtil.toCode(code, MarketMapper.toExchangeIdOrNull(market))

    private fun asObservableSource(value: String) =
        ObservableSource.of(value.toUpperCase().replace(regex, "-"))
}