package com.rkfcheung.quoteengine.client

import com.influxdb.client.InfluxDBClient
import com.influxdb.client.domain.DeletePredicateRequest
import com.influxdb.exceptions.InfluxException
import com.influxdb.query.FluxTable
import com.opengamma.strata.basics.currency.Currency
import com.opengamma.strata.data.ObservableSource
import com.opengamma.strata.product.ProductType
import com.rkfcheung.quoteengine.config.InfluxProperties
import com.rkfcheung.quoteengine.connector.RangeQuoteConnector
import com.rkfcheung.quoteengine.model.definition.*
import com.rkfcheung.quoteengine.model.price.CandleVPrice
import com.rkfcheung.quoteengine.model.price.Price
import com.rkfcheung.quoteengine.model.price.measurement.PriceCurrencyMeasurement
import com.rkfcheung.quoteengine.model.price.measurement.PriceMeasurement
import com.rkfcheung.quoteengine.model.quote.QuoteRequest
import com.rkfcheung.quoteengine.model.quote.RangeQuoteRequest
import com.rkfcheung.quoteengine.model.ticker.Ticker
import com.rkfcheung.quoteengine.model.ticker.measurement.TickerMeasurement
import com.rkfcheung.quoteengine.util.DateTimeUtil
import com.rkfcheung.quoteengine.util.DateTimeUtil.asZonedDateTime
import com.rkfcheung.quoteengine.util.TickerUtil
import com.rkfcheung.quoteengine.util.YahooUtil
import java.time.LocalDate
import java.time.ZoneId
import java.time.ZonedDateTime
import kotlin.math.max
import kotlin.math.min

class InfluxPriceClient(val client: InfluxDBClient, private val properties: InfluxProperties) :
    RangeQuoteConnector<RangeQuoteRequest, CandleVPrice>() {
    override val parameterSource: ObservableSource = ObservableSource.NONE
    override val productType: ProductType = ProductType.OTHER
    private val bucket = properties.bucket
    private val since = LocalDate.of(1871, 1, 1).asZonedDateTime()

    override fun quote(request: RangeQuoteRequest): List<CandleVPrice> {
        val prices = mutableListOf<CandleVPrice>()
        var start = DateTimeUtil.max(request.startTime, since)
        val stop = DateTimeUtil.max(request.endTime, since)
        while (start.isBefore(stop)) {
            val endTime = DateTimeUtil.min(start.plusYears(3L), stop)
            val subRequest =
                RangeQuoteRequest(request.resolution, start, endTime, request.parameterCode, request.parameterSource)
            prices += doQuote(subRequest)
            start = endTime
        }
        return prices
    }

    inline fun <reified M : PriceMeasurement> measurements(
        request: QuoteRequest,
        fields: List<String> = emptyList()
    ): List<M> =
        client.queryApi.query(prepareQuery(request, fields = fields, source = request.parameterSource), M::class.java)

    fun delete(price: Price) {
        val tags = price.getMeasurementTags().map { "\"${it.key}\"=\"${it.value}\"" }.joinToString(" AND ")
        val request = DeletePredicateRequest().apply {
            stop = price.timestamp.toOffsetDateTime()
            start = stop.minus(price.resolution.asDuration())
            predicate = "_measurement=\"${MeasurementBasics.PRICE}\" AND $tags"
        }
        log.info("Deleting $request ...")
        client.deleteApi.delete(request, bucket, properties.org)
    }

    fun save(prices: List<Price>) {
        if (prices.isEmpty()) {
            return
        }
        val points = prices.filterNot { it.value == 0.0 }.map { it.asPoint() }
        var i = 0
        val j = points.size - 1
        while (i < j) {
            val k = min(j, i + 365)
            client.writeApi.writePoints(points.subList(i, k))
            i = k + 1
        }
    }

    fun save(ticker: Ticker) {
        save(ticker.asTickerMeasurement())
    }

    fun save(ticker: TickerMeasurement) {
        client.writeApi.writeMeasurement(MeasurementBasics.WRITE_PRECISION, ticker)
    }

    fun ticker(code: String, years: Int = 1): Ticker? {
        val yhCode = YahooUtil.toCode(code)
        val yhFilter = "r[\"${MeasurementBasics.CODE}\"] == \"$yhCode\""
        val bbCode = YahooUtil.toBloombergCode(code) ?: code.replace(".", ":")
        val bbFilter = if (bbCode.contains(":")) {
            "r[\"_field\"] == \"${MeasurementBasics.BLOOMBERG_CODE}\" and r[\"_value\"] == \"$bbCode\""
        } else {
            ""
        }
        val query = prepareTickerQuery(yhFilter, years)
        val tables = mutableListOf<FluxTable>()
        tables += try {
            client.queryApi.query(query)
        } catch (e: InfluxException) {
            log.warn("Failed to search $yhCode!")
            return null
        }
        if (tables.isEmpty() && bbFilter.isNotEmpty()) {
            tables += try {
                client.queryApi.query(prepareTickerQuery(bbFilter, years))
            } catch (e: InfluxException) {
                log.warn("Failed to search $bbCode!")
                return null
            }
        }
        if (tables.isEmpty()) {
            return null
        }
        val measurement = TickerMeasurement(yhCode, ZonedDateTime.now().minusYears(years.toLong()).toInstant())
        measurement.update(tables)
        log.info("Found $measurement")
        return measurement.asTicker()
    }

    fun prepareQuery(
        request: QuoteRequest,
        measurement: String = MeasurementBasics.PRICE,
        fields: List<String> = emptyList(),
        source: ObservableSource? = null
    ): String {
        val ticker = request.parameterCode
        val startTime = if (request is RangeQuoteRequest) request.startTime else request.timestamp.minusYears(1L)
        val start = DateTimeUtil.max(startTime, since)
        val stop = DateTimeUtil.max(request.timestamp, since)
        val fieldFilter = fields.joinToString(" ") { "or r[\"_field\"] == \"$it\"" }
        val sourceFilter = if (source != null) {
            "|> filter(fn: (r) => r[\"${MeasurementBasics.PARAMETER_SOURCE}\"] == \"$source\")"
        } else {
            ""
        }
        return """
from(bucket: "$bucket")
  |> range(start: ${toOffsetDateTime(start)}, stop: ${toOffsetDateTime(stop)})
  |> filter(fn: (r) => r["_measurement"] == "$measurement")
  |> filter(fn: (r) => r["${MeasurementBasics.CODE}"] == "${ticker.asMeasurementCode()}") 
  $sourceFilter
  |> filter(fn: (r) => r["${MeasurementBasics.RESOLUTION}"] == "${request.resolution}") 
  |> filter(fn: (r) => r["_field"] == "${MeasurementBasics.VALUE}" $fieldFilter) 
  |> yield(name: "last")
            """.trimIndent().also { log.info("Query $it") }
    }

    private fun doQuote(request: RangeQuoteRequest): List<CandleVPrice> {
        val measurements = measurements<PriceCurrencyMeasurement>(request, MeasurementBasics.ALL_PRICE_FIELDS)
        val closeAttributes = setOf(PriceAttribute.ADJ_CLOSE.name, PriceAttribute.CLOSE.name)
        return measurements.groupBy { it.time }.map { (time, prices) ->
            val ref = prices.first { it.field == MeasurementBasics.VALUE }
            val value = ref.value
            val currency = ref.currency?.let { Currency.of(it) } ?: Currency.XXX
            val priceValues = PriceAttribute.values().map { a ->
                val found = prices.filter { p ->
                    (p.field == a.name && (p.value != value || (closeAttributes.contains(p.field))))
                }.map { it.value }
                a to when (a) {
                    PriceAttribute.HIGH -> found.maxOrNull()
                    PriceAttribute.LOW -> found.minOrNull()
                    else -> found.firstOrNull()
                }
            }.toMap()
            CandleVPrice(
                Resolution.valueOf(ref.resolution),
                ObservableSource.of(ref.parameterSource),
                LocalDate.ofInstant(time, ZoneId.systemDefault()),
                TickerUtil.asTicker(ref.code),
                priceValues[PriceAttribute.OPEN] ?: value,
                priceValues[PriceAttribute.HIGH] ?: value,
                priceValues[PriceAttribute.LOW] ?: value,
                priceValues[PriceAttribute.CLOSE] ?: value,
                priceValues[PriceAttribute.VOLUME] ?: 0.0,
                currency
            ).apply {
                priceValues[PriceAttribute.ADJ_CLOSE]?.let { adjClose = it }
                priceValues[PriceAttribute.ASK]?.let { ask = it }
                priceValues[PriceAttribute.BID]?.let { bid = it }
                priceValues[PriceAttribute.TURNOVER]?.let { turnover = it }
            }
        }
    }

    private fun prepareTickerQuery(codeFilter: String, years: Int = 1) = """
from(bucket: "$bucket")
  |> range(start: -${max(1, years)}y)
  |> filter(fn: (r) => r["_measurement"] == "${Entities.TickerEntity}")
  |> filter(fn: (r) => $codeFilter)
  |> yield(name: "last")
                """.trimIndent().also { log.info("Query $it") }

    private fun toOffsetDateTime(value: ZonedDateTime): String {
        return value.toOffsetDateTime().toString().replace("T00:00+", "T00:00:00+")
            .replace("+07:36:42", "+08:00")
    }
}