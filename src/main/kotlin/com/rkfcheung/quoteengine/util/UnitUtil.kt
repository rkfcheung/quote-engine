package com.rkfcheung.quoteengine.util

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.opengamma.strata.basics.currency.Currency
import com.opengamma.strata.basics.currency.CurrencyPair
import com.opengamma.strata.data.ObservableSource
import com.opengamma.strata.product.ProductType
import com.rkfcheung.quoteengine.model.definition.Resolution
import com.rkfcheung.quoteengine.model.price.CandleVPrice
import com.rkfcheung.quoteengine.model.ticker.ForexTicker
import com.rkfcheung.quoteengine.model.ticker.Ticker
import org.ta4j.core.Bar
import org.ta4j.core.num.PrecisionNum
import java.time.Duration

object UnitUtil {
    const val K = 1_000

    fun Bar.asCandleVPrice(
        parameterSource: ObservableSource = ObservableSource.NONE,
        parameterCode: Ticker = Ticker(ProductType.OTHER, MarketMapper.XX),
        currency: Currency = Currency.XXX
    ): CandleVPrice =
        CandleVPrice(
            timePeriod.asResolution(),
            parameterSource,
            endTime.toLocalDate(),
            parameterCode,
            openPrice.doubleValue(),
            highPrice.doubleValue(),
            lowPrice.doubleValue(),
            closePrice.doubleValue(),
            toDouble(volume.doubleValue()),
            currency
        )

    fun Double.asPrecisionNum(): PrecisionNum = PrecisionNum.valueOf(this)

    fun Duration.asResolution(): Resolution = when (this) {
        Duration.ofNanos(1L) -> Resolution.TICK
        Duration.ofMinutes(1L) -> Resolution.MINUTE
        Duration.ofHours(1L) -> Resolution.HOURLY
        Duration.ofDays(1L) -> Resolution.DAILY
        Duration.ofDays(7L) -> Resolution.WEEKLY
        Duration.ofDays(30L) -> Resolution.MONTHLY
        Duration.ofDays(120L) -> Resolution.QUARTERLY
        Duration.ofDays(365L) -> Resolution.ANNUAL
        else -> Resolution.NONE
    }

    fun CurrencyPair.asForexTicker() = ForexTicker(base, counter)

    fun asJson(value: Any) = jacksonObjectMapper().writeValueAsString(value)

    fun mid(bid: Double, ask: Double): Double {
        if (bid == 0.0) {
            return ask
        } else if (ask == 0.0) {
            return bid
        }
        return (bid + ask) / 2
    }

    fun toDouble(value: Double, divisor: Double = K.toDouble()): Double = value / divisor

    fun toDouble(value: Long, divisor: Double = K.toDouble()): Double = toDouble(value.toDouble(), divisor)
}