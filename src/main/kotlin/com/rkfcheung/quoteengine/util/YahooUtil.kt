package com.rkfcheung.quoteengine.util

import com.jimmoores.quandl.Frequency
import com.opengamma.strata.basics.currency.Currency
import com.opengamma.strata.product.common.ExchangeId
import com.opengamma.strata.product.common.ExchangeIds
import com.rkfcheung.quoteengine.model.definition.ParameterSource
import com.rkfcheung.quoteengine.model.definition.Resolution
import com.rkfcheung.quoteengine.model.price.CandleVPrice
import com.rkfcheung.quoteengine.model.price.ForexPrice
import com.rkfcheung.quoteengine.model.ticker.Ticker
import com.rkfcheung.quoteengine.util.DateTimeUtil.asLocalDate
import yahoofinance.histquotes.HistoricalQuote
import yahoofinance.histquotes.Interval
import yahoofinance.quotes.fx.FxQuote
import java.time.ZonedDateTime

object YahooUtil {
    fun Frequency.asInterval() = when (this) {
        Frequency.DAILY -> Interval.DAILY
        Frequency.WEEKLY -> Interval.WEEKLY
        Frequency.MONTHLY -> Interval.MONTHLY
        else -> throw IllegalArgumentException("Unable to convert Frequency $this to Interval")
    }

    fun FxQuote.asForexPrice(): ForexPrice {
        val code = TickerUtil.asForexTicker(symbol)
        val value = price.toDouble()
        return ForexPrice(
            Resolution.MINUTE,
            ParameterSource.YAHOO,
            ZonedDateTime.now(),
            code,
            value,
            value
        )
    }

    fun HistoricalQuote.asCandleVPrice(
        resolution: Resolution,
        parameterCode: Ticker,
        currency: Currency
    ): CandleVPrice {
        return CandleVPrice(
            resolution,
            ParameterSource.YAHOO,
            date.asLocalDate(),
            parameterCode,
            open?.toDouble() ?: 0.0,
            high?.toDouble() ?: 0.0,
            low?.toDouble() ?: 0.0,
            close?.toDouble() ?: 0.0,
            UnitUtil.toDouble(volume ?: 0L),
            currency
        ).apply { adjClose = this@asCandleVPrice.adjClose?.toDouble() ?: close }
    }

    fun toBloombergCode(code: String): String? {
        val forexTicker = TickerUtil.toForexTickerOrNull(code)
        if (forexTicker != null) {
            return "${forexTicker.base}${forexTicker.counter}${TickerUtil.CUR_TYPE}"
        }
        val pair = toCodePair(code) ?: return null
        val prefix = pair.first
        val suffix = pair.second
        val codeAsInt = toIntOrNull(prefix, code)
        return when {
            MarketMapper.isCHN(suffix) && codeAsInt != null -> "$prefix:${MarketMapper.CH}"
            MarketMapper.isHKG(suffix) && codeAsInt != null -> "$codeAsInt:$suffix"
            code.contains(':') -> "$prefix:$suffix"
            else -> null
        }
    }

    fun toBloombergCode(code: String, mic: ExchangeId?): String? {
        return toCodeMic(code, mic)?.let {
            val codeAsInt = it.first
            when (val exchange = it.second) {
                ExchangeIds.XHKG -> "$codeAsInt:${MarketMapper.HK}"
                MarketMapper.XSHE, MarketMapper.XSHG -> "${MarketMapper.toCode(exchange, codeAsInt)}:${MarketMapper.CH}"
                else -> null
            }
        }
    }

    fun toCode(base: Currency, counter: Currency): String {
        return "$base$counter${TickerUtil.FX_TYPE}"
    }

    fun toCode(code: String, mic: ExchangeId? = null): String {
        return toCodeMic(code, mic)?.let {
            val codeAsInt = it.first
            when (val exchange = it.second) {
                ExchangeIds.XHKG -> "${MarketMapper.toCode(exchange, codeAsInt)}.${MarketMapper.HK}"
                MarketMapper.XSHE -> "${MarketMapper.toCode(exchange, codeAsInt)}.${MarketMapper.SZ}"
                MarketMapper.XSHG -> "${MarketMapper.toCode(exchange, codeAsInt)}.${MarketMapper.SS}"
                else -> null
            }
        } ?: code
    }

    fun toCodePair(code: String): Pair<String, String>? {
        listOf(".", ":").forEach {
            val parts = code.split(it)
            if (parts.size == 2) {
                return Pair(parts.first().toUpperCase(), parts.last().toUpperCase())
            }
        }

        return null
    }

    fun toCodeMic(code: String, mic: ExchangeId? = null): Pair<Int, ExchangeId>? {
        val pair = toCodePair(code)
        val codeAsInt = toIntOrNull(pair?.first, code)
        val exchange = mic ?: pair?.second?.let { MarketMapper.toExchangeIdOrNull(it) }
        return if (codeAsInt != null && exchange != null) {
            Pair(codeAsInt, exchange)
        } else {
            null
        }
    }

    private fun toIntOrNull(first: String?, second: String?) = (first ?: second)?.trimStart('0')?.toIntOrNull()
}