package com.rkfcheung.quoteengine.util

import com.opengamma.strata.basics.currency.Currency
import com.opengamma.strata.basics.currency.CurrencyPair
import com.opengamma.strata.basics.location.Country
import com.opengamma.strata.product.ProductType
import com.opengamma.strata.product.common.ExchangeIds
import com.rkfcheung.quoteengine.model.definition.BloombergSecurityType
import com.rkfcheung.quoteengine.model.definition.ExtraTypedConstants
import com.rkfcheung.quoteengine.model.entity.AssetClass
import com.rkfcheung.quoteengine.model.entity.TickerEntity
import com.rkfcheung.quoteengine.model.ticker.EquityTicker
import com.rkfcheung.quoteengine.model.ticker.ForexTicker
import com.rkfcheung.quoteengine.model.ticker.Ticker
import com.rkfcheung.quoteengine.util.UnitUtil.asForexTicker
import kotlin.math.max

object TickerUtil {
    const val FX_TYPE = "=X"
    const val CUR_TYPE = ":CUR"

    fun asCurrency(code: String, defaultValue: Currency = Currency.XXX): Currency = try {
        Currency.parse(code)
    } catch (e: IllegalArgumentException) {
        defaultValue
    }

    fun asForexTicker(code: String): ForexTicker {
        val fxCode = code.replace(CUR_TYPE, FX_TYPE).toUpperCase()
        if (fxCode.length != 8) {
            throw IllegalArgumentException("Invalid Code: $code")
        }
        val base = Currency.of(fxCode.substring(0, 3))
        val counter = Currency.of(fxCode.substring(3, 6))
        return ForexTicker(base, counter)
    }

    fun asProductType(assetClass: AssetClass): ProductType {
        return when (assetClass.id) {
            1 -> ProductType.SECURITY
            2 -> ProductType.ETD_OPTION
            3 -> ProductType.FX_SINGLE
            4 -> ExtraTypedConstants.PENSION_FUND
            5 -> ExtraTypedConstants.INSURANCE
            6 -> ExtraTypedConstants.FUND
            7 -> ExtraTypedConstants.INDEX
            8 -> ExtraTypedConstants.SELF_FUND
            9 -> ExtraTypedConstants.SELF_MARKET_INDEX
            18 -> ExtraTypedConstants.SELF_ECONOMIC_INDEX
            else -> ProductType.of(assetClass.name.replace(" ", ""))
        }
    }

    fun asProductType(code: String, defaultValue: ProductType = ProductType.OTHER): ProductType {
        val pair = YahooUtil.toCodeMic(code)
        val codeAsInt = pair?.first
        val exchange = pair?.second
        if (exchange == ExchangeIds.XHKG && codeAsInt != null) {
            // https://www.hkex.com.hk/-/media/HKEX-Market/Products/Securities/Stock-Code-Allocation-Plan/scap.pdf
            if (codeAsInt in 4000..4299 || codeAsInt in 6750..6799 || codeAsInt in 86600..86799) {
                return ProductType.BOND
            } else if (codeAsInt in 10000..29999 || codeAsInt in 47000..48999 || codeAsInt in 50000..69999 || codeAsInt in 89000..89999) {
                return ProductType.ETD_OPTION
            }
        }
        return when {
            code.startsWith("^") -> ExtraTypedConstants.INDEX
            code.startsWith("$") -> ExtraTypedConstants.SELF_MARKET_INDEX
            isForexTicker(code) -> ProductType.FX_SINGLE
            else -> defaultValue
        }
    }

    fun asTicker(code: String, defaultValue: ProductType = ProductType.OTHER): Ticker =
        toTickerOrNull(code) ?: Ticker(asProductType(code, defaultValue), code)

    fun pretty(entity: TickerEntity): TickerEntity {
        val ticker = entity.asTicker()
        if (ticker is ForexTicker) {
            return ticker.asTickerEntity()
        }
        val market = if (entity.market.isNotEmpty()) {
            entity.market
        } else {
            MarketMapper.toMarket(entity.code) ?: MarketMapper.XX
        }
        val currency = toCurrency(ticker.code, market) ?: asCurrency(entity.currency)
        val lotSize = max(1, entity.lotSize)
        return TickerEntity(entity.code, entity.name, currency.code, market, lotSize)
    }

    fun toCurrency(code: String, market: String? = null): Currency? {
        val pair = YahooUtil.toCodeMic(code)
        val codeAsInt = pair?.first
        val exchange = pair?.second
        val mkt = market ?: exchange?.name ?: YahooUtil.toCodePair(code)?.second ?: MarketMapper.XX
        return if ((MarketMapper.isHKG(exchange) || MarketMapper.isHKG(mkt)) && codeAsInt != null) {
            // https://www.hkex.com.hk/-/media/HKEX-Market/Products/Securities/Stock-Code-Allocation-Plan/scap.pdf
            when (codeAsInt) {
                in 9000..9599 -> Currency.USD
                in 80000..89999 -> Currency.CNY
                else -> Currency.HKD
            }
        } else {
            null
        }
    }

    fun toEquityTickerOrNull(code: String): Ticker? {
        val pair = YahooUtil.toCodePair(code) ?: return null
        val prefix = pair.first
        val suffix = pair.second
        val country = MarketMapper.toCountryOrNull(suffix)
        val codeAsInt = prefix.toIntOrNull()
        val defaultType = if (country != null) ProductType.SECURITY else ProductType.OTHER
        val productType = asProductType(code, defaultType)
        if (country == Country.HK && codeAsInt != null && productType != ProductType.SECURITY) {
            return Ticker(productType, code)
        }
        return MarketMapper.toExchangeIdOrNull(suffix)?.let { EquityTicker(it, code) }
    }

    fun toForexTickerOrNull(code: String): ForexTicker? {
        return when {
            code.endsWith(FX_TYPE) || code.endsWith(CUR_TYPE) -> {
                try {
                    asForexTicker(code)
                } catch (e: IllegalArgumentException) {
                    null
                }
            }
            code.length == 7 -> {
                try {
                    CurrencyPair.parse(code)
                } catch (e: IllegalArgumentException) {
                    null
                }?.asForexTicker()
            }
            else -> null
        }
    }

    fun toProductTypeOrNull(securityType: String): ProductType? =
        enumValues<BloombergSecurityType>().find { it.name == securityType.toUpperCase().replace(" ", "_") }
            ?.asProductType()

    fun toTickerOrNull(code: String): Ticker? = toForexTickerOrNull(code) ?: toEquityTickerOrNull(code)

    private fun isForexTicker(code: String): Boolean = toForexTickerOrNull(code) != null
}