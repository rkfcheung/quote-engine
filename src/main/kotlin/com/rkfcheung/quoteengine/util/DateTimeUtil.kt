package com.rkfcheung.quoteengine.util

import java.time.Instant
import java.time.LocalDate
import java.time.ZoneId
import java.time.ZonedDateTime
import java.util.*

object DateTimeUtil {
    fun Calendar.asLocalDate(): LocalDate = LocalDate.ofInstant(this.toInstant(), ZoneId.systemDefault())

    fun Instant.asZonedDateTime(): ZonedDateTime = ZonedDateTime.ofInstant(this, ZoneId.systemDefault())

    fun LocalDate.asInstant(): Instant = asZonedDateTime().toInstant()

    fun LocalDate.asZonedDateTime(): ZonedDateTime = atStartOfDay(ZoneId.systemDefault())

    fun ZonedDateTime.asCalendar(): Calendar = GregorianCalendar.from(this)

    fun min(a: ZonedDateTime, b: ZonedDateTime): ZonedDateTime = if (a.isBefore(b)) a else b

    fun max(a: ZonedDateTime, b: ZonedDateTime): ZonedDateTime = if (a.isBefore(b)) b else a
}