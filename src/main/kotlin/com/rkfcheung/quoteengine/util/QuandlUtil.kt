package com.rkfcheung.quoteengine.util

import com.jimmoores.quandl.Frequency
import com.jimmoores.quandl.HeaderDefinition
import com.jimmoores.quandl.Row
import com.jimmoores.quandl.TabularResult
import com.opengamma.strata.basics.currency.Currency
import com.opengamma.strata.product.common.ExchangeId
import com.rkfcheung.quoteengine.model.definition.ParameterSource
import com.rkfcheung.quoteengine.model.definition.Resolution
import com.rkfcheung.quoteengine.model.price.CandleVPrice
import com.rkfcheung.quoteengine.model.ticker.Ticker

object QuandlUtil {
    private const val DATE = "Date"
    private const val OPEN = "Open"
    private const val HIGH = "High"
    private const val LOW = "Low"
    private const val CLOSE = "Close"
    private const val VOLUME = "Volume"
    private const val NOMINAL_PRICE = "Nominal Price"
    private const val SHARE_VOLUME = "Share Volume (000)"
    private const val TURNOVER = "Turnover (000)"
    const val API_KEY = "API_KEY"

    fun Frequency.asResolution(): Resolution = when (this) {
        Frequency.DAILY -> Resolution.DAILY
        Frequency.WEEKLY -> Resolution.WEEKLY
        Frequency.MONTHLY -> Resolution.MONTHLY
        Frequency.QUARTERLY -> Resolution.QUARTERLY
        Frequency.ANNUAL -> Resolution.ANNUAL
        else -> Resolution.NONE
    }

    fun TabularResult.asPrices(resolution: Resolution, parameterCode: Ticker, currency: Currency): List<CandleVPrice> {
        return this.map { it.asCandleVPrice(headerDefinition, resolution, parameterCode, currency) }
    }

    fun Row.asCandleVPrice(
        header: HeaderDefinition, resolution: Resolution, parameterCode: Ticker, currency: Currency
    ): CandleVPrice {
        val close = getDouble(this, header, CLOSE) ?: getDouble(this, header, NOMINAL_PRICE) ?: 0.0
        val high = getDouble(this, header, HIGH) ?: close
        val low = getDouble(this, header, LOW) ?: close
        val open = getDouble(this, header, OPEN) ?: (high + low) / 2
        val volChk = getDouble(this, header, VOLUME) ?: 0.0
        val volume = if (volChk > 0.0) {
            UnitUtil.toDouble(volChk)
        } else {
            getDouble(this, header, SHARE_VOLUME) ?: 0.0
        }
        val turnoverValue = getDouble(this, header, TURNOVER) ?: 0.0
        return CandleVPrice(
            resolution,
            ParameterSource.QUANDL,
            getLocalDate(DATE),
            parameterCode,
            open,
            high,
            low,
            close,
            volume,
            currency
        ).apply {
            if (turnoverValue > 0.0) {
                turnover = turnoverValue
            }
        }
    }

    fun toCode(code: String, mic: ExchangeId? = null): String? {
        if (code.contains('/')) {
            return code
        }
        val pair = YahooUtil.toCodeMic(code, mic)
        val codeAsInt = pair?.first
        val exchange = pair?.second
        if (MarketMapper.isHKG(exchange) && codeAsInt != null) {
            return "${MarketMapper.HKEX}/${MarketMapper.toCode(codeAsInt, 5)}"
        }
        return null
    }

    private fun getDouble(row: Row, header: HeaderDefinition, column: String): Double? {
        return if (header.columnNames.contains(column)) {
            row.getDouble(column)
        } else {
            null
        }
    }
}