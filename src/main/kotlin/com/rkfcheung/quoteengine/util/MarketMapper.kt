package com.rkfcheung.quoteengine.util

import com.opengamma.strata.basics.location.Country
import com.opengamma.strata.product.common.ExchangeId
import com.opengamma.strata.product.common.ExchangeIds

object MarketMapper {
    const val CH = "CH"
    const val HKEX = "HKEX"
    const val SS = "SS"
    const val SZ = "SZ"
    const val WIKI = "WIKI"
    const val XX = "XX"
    val CN: Country = Country.CN
    val HK: Country = Country.HK
    val ARCX: ExchangeId = ExchangeId.of("ARCX")
    val XASE: ExchangeId = ExchangeId.of("XASE")
    val XBOS: ExchangeId = ExchangeId.of("XBOS")
    val XDUB: ExchangeId = ExchangeId.of("XDUB")
    val XKRX: ExchangeId = ExchangeId.of("XKRX")
    val XLUX: ExchangeId = ExchangeId.of("XLUX")
    val XNAS: ExchangeId = ExchangeId.of("XNAS")
    val XNCM: ExchangeId = ExchangeId.of("XNCM")
    val XNGS: ExchangeId = ExchangeId.of("XNGS")
    val XNIM: ExchangeId = ExchangeId.of("XNIM")
    val XNMS: ExchangeId = ExchangeId.of("XNMS")
    val XNYS: ExchangeId = ExchangeId.of("XNYS")
    val XPSX: ExchangeId = ExchangeId.of("XPSX")
    val XSHE: ExchangeId = ExchangeId.of("XSHE")
    val XSHG: ExchangeId = ExchangeId.of("XSHG")

    private const val HONG_KONG = "Hong Kong"
    private const val SHANGHAI = "Shanghai"
    private const val SHENZHEN = "Shenzhen"
    private val CHN_EXCHANGE = setOf(XSHE, XSHG)
    private val HKG_EXCHANGE = setOf(ExchangeIds.XHKG)
    private val CHN_MARKET = setOf(CN.code, CN.code3Char, SHANGHAI, SHENZHEN, SS, SZ) + CHN_EXCHANGE.map { it.name }
    private val HKG_MARKET = setOf(HK.code, HK.code3Char, HONG_KONG, HKEX) + HKG_EXCHANGE.map { it.name }
    private val exchangeToMarket = mapOf<ExchangeId, String>(
        ARCX to "UP",
        XASE to "XN",
        XBOS to "UB",
        XDUB to "IR",
        XKRX to Country.KR.code,
        XLUX to "LX",
        XNAS to "UD",
        XNCM to "UR",
        XNGS to "UW",
        XNIM to "UT",
        XNMS to "UQ",
        XNYS to "UN",
        XPSX to "UX",
        XSHE to SZ,
        XSHG to SS,
        ExchangeIds.XHKG to HK.code,
        ExchangeIds.XSES to "SI",
        ExchangeIds.XTKS to Country.JP.code
    )
    private val marketToExchange = exchangeToMarket.map { it.value to it.key }.toMap()

    fun isCHN(market: String): Boolean {
        return CHN_MARKET.contains(toExchangeIdOrNull(market)?.name ?: market)
    }

    fun isHKG(mic: ExchangeId?): Boolean = HKG_EXCHANGE.contains(mic)

    fun isHKG(market: String): Boolean {
        return HKG_MARKET.contains(market)
    }

    fun isUSA(market: String): Boolean {
        return market == Country.US.code
    }

    fun toCode(code: Int, zeros: Int): String {
        return if (zeros > 0) String.format("%0${zeros}d", code) else code.toString()
    }

    fun toCode(mic: ExchangeId, code: Int, zeros: Int = 4): String {
        return when (mic) {
            ExchangeIds.XHKG -> toCode(code, zeros)
            XSHE, XSHG -> toCode(code, 6)
            else -> code.toString()
        }
    }

    fun toCode(mic: ExchangeId, code: String, zeros: Int = 4): String {
        val codeAsInt = code.toIntOrNull() ?: return code
        return toCode(mic, codeAsInt, zeros)
    }

    fun toCountryOrNull(code: String): Country? = try {
        Country.parse(code)
    } catch (e: IllegalArgumentException) {
        try {
            Country.of3Char(code)
        } catch (e: IllegalArgumentException) {
            when {
                isCHN(code) -> Country.CN
                isHKG(code) -> Country.HK
                else -> null
            }
        }
    }

    fun toExchangeIdOrNull(market: String): ExchangeId? {
        if (isHKG(market)) {
            return ExchangeIds.XHKG
        }
        return when (market) {
            "ID" -> XDUB
            "Japan" -> ExchangeIds.XTKS
            "Luxembourg" -> XLUX
            "Korea (South)" -> XKRX
            "CG", "SHA", SS, SHANGHAI -> XSHG
            "CS", "SHE", SZ, SHENZHEN -> XSHE
            "NASDAQ GM" -> XNAS
            "NYSE Arca" -> ARCX
            "XS" -> ExchangeIds.XSES
            else -> marketToExchange[market]
        }
    }

    fun toMarket(code: String): String? {
        val market = YahooUtil.toCodePair(code)?.second
        if (market?.length == 2) {
            return market
        }
        return YahooUtil.toCodeMic(code)?.second?.let { toMarket(it) }
    }

    fun toMarket(mic: ExchangeId): String? = exchangeToMarket[mic] ?: toCountryOrNull(mic.name.substring(1))?.code

    fun toMarket(code: String, mic: ExchangeId?): String? = toMarket(code) ?: mic?.let { toMarket(it) }
}