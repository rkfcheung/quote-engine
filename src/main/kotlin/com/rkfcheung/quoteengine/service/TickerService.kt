package com.rkfcheung.quoteengine.service

import com.rkfcheung.quoteengine.client.FinnhubClient
import com.rkfcheung.quoteengine.client.InfluxPriceClient
import com.rkfcheung.quoteengine.connector.TickerEntityConnector
import com.rkfcheung.quoteengine.model.ticker.Ticker
import com.rkfcheung.quoteengine.util.TickerUtil
import com.rkfcheung.quoteengine.util.YahooUtil
import kotlinx.coroutines.runBlocking
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.cache.annotation.Cacheable
import org.springframework.stereotype.Service

@Service
class TickerService {
    @Autowired
    private lateinit var finnhubClient: FinnhubClient

    @Autowired
    private lateinit var influxPriceClient: InfluxPriceClient

    @Autowired
    private lateinit var tickerEntityConnector: TickerEntityConnector

    private val log: Logger = LoggerFactory.getLogger(this.javaClass)

    @Cacheable
    fun search(code: String): Ticker? {
        influxPriceClient.ticker(code, 1)?.let { ticker ->
            log.info("Found ${ticker.description()}")
            val pair = YahooUtil.toCodePair(code)
            if (pair != null && (ticker.micCode() == null || ticker.currency() == null || ticker.figiCode() == null)) {
                val measurement = ticker.asTickerMeasurement()
                val enriched = enrich(code)
                if (enriched != null) {
                    measurement.update(enriched)
                    influxPriceClient.save(measurement)
                    return measurement.asTicker()
                }
            }
            return ticker
        }

        val ticker = tickerEntityConnector.getTicker(code) ?: TickerUtil.toTickerOrNull(code) ?: enrich(code)
        if (ticker != null) {
            influxPriceClient.save(ticker)
        }
        return ticker
    }

    @Cacheable
    private fun enrich(code: String): Ticker? = runBlocking {
        return@runBlocking finnhubClient.searchTicker(code)
    }
}