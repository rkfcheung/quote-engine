package com.rkfcheung.quoteengine.service

import com.opengamma.strata.product.ProductType
import com.rkfcheung.quoteengine.client.FinnhubClient
import com.rkfcheung.quoteengine.client.InfluxPriceClient
import com.rkfcheung.quoteengine.connector.HistPriceConnector
import com.rkfcheung.quoteengine.connector.YahooEquityConnector
import com.rkfcheung.quoteengine.model.definition.ParameterSource
import com.rkfcheung.quoteengine.model.price.CandleVPrice
import com.rkfcheung.quoteengine.model.quote.RangeQuoteRequest
import com.rkfcheung.quoteengine.model.quote.YahooQuoteRequest
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.cache.annotation.Cacheable
import org.springframework.stereotype.Service
import java.time.LocalDate
import java.time.temporal.ChronoUnit
import kotlin.math.max

@Service
class QuoteService {
    @Autowired
    private lateinit var finnhubClient: FinnhubClient

    @Autowired
    private lateinit var influxPriceClient: InfluxPriceClient

    @Autowired
    private lateinit var histPriceConnector: HistPriceConnector

    @Autowired
    private lateinit var yahooEquityConnector: YahooEquityConnector

    private val log: Logger = LoggerFactory.getLogger(this.javaClass)
    private val threshold = 0.67

    @Cacheable
    fun quote(request: RangeQuoteRequest): List<CandleVPrice> {
        log.info("Requesting ${request.parameterCode.description()} ...")
        val series = priceMap(influxPriceClient.quote(request))
        val seriesToSave = mutableMapOf<LocalDate, CandleVPrice>()
        if (toRequest(request, series)) {
            val seriesStartTime = series.values.firstOrNull()?.timestamp?.plusDays(1L)
            val seriesEndTime = series.values.lastOrNull()?.timestamp?.minusDays(1L)
            log.info("Requesting on ${ParameterSource.MYSQL} (size = ${series.size}, $seriesStartTime to $seriesEndTime) ...")
            val prices = if (seriesStartTime != null && seriesEndTime != null) {
                histPriceConnector.quote(
                    RangeQuoteRequest(
                        request.resolution,
                        request.startTime,
                        seriesStartTime,
                        request.parameterCode,
                        ParameterSource.MYSQL
                    )
                ) + histPriceConnector.quote(
                    RangeQuoteRequest(
                        request.resolution,
                        seriesStartTime,
                        seriesEndTime,
                        request.parameterCode,
                        ParameterSource.MYSQL
                    )
                ) + histPriceConnector.quote(
                    RangeQuoteRequest(
                        request.resolution,
                        seriesEndTime,
                        request.endTime,
                        request.parameterCode,
                        ParameterSource.MYSQL
                    )
                )
            } else {
                histPriceConnector.quote(request)
            }
            seriesToSave += priceMap(prices)
            series += seriesToSave
        }
        if (toRequest(request, series) && request.parameterCode.type != ProductType.FX_SINGLE) {
            val seriesStartTime = series.values.firstOrNull()?.timestamp?.plusDays(1L)
            val seriesEndTime = series.values.lastOrNull()?.timestamp?.minusDays(1L)
            log.info("Requesting on ${ParameterSource.YAHOO} (size = ${series.size}, $seriesStartTime to $seriesEndTime) ...")
            val prices = if (seriesStartTime != null && seriesEndTime != null) {
                yahooEquityConnector.quote(
                    YahooQuoteRequest(
                        request.resolution,
                        request.startTime,
                        seriesStartTime,
                        request.parameterCode
                    )
                ) + yahooEquityConnector.quote(
                    YahooQuoteRequest(
                        request.resolution,
                        seriesStartTime,
                        seriesEndTime,
                        request.parameterCode
                    )
                )+ yahooEquityConnector.quote(
                    YahooQuoteRequest(
                        request.resolution,
                        seriesEndTime,
                        request.endTime,
                        request.parameterCode
                    )
                )
            } else {
                yahooEquityConnector.quote(YahooQuoteRequest(request))
            }
            seriesToSave += priceMap(prices)
            series += seriesToSave
        }
        if (toRequest(request, series) && request.parameterCode.type == ProductType.FX_SINGLE) {
            val seriesStartTime = series.values.firstOrNull()?.timestamp?.plusDays(1L)
            val seriesEndTime = series.values.lastOrNull()?.timestamp?.minusDays(1L)
            log.info("Requesting on ${ParameterSource.FINNHUB} (size = ${series.size}, $seriesStartTime to $seriesEndTime) ...")
            val prices = if (seriesStartTime != null && seriesEndTime != null) {
                finnhubClient.quote(
                    RangeQuoteRequest(
                        request.resolution,
                        request.startTime,
                        seriesStartTime,
                        request.parameterCode
                    )
                ) + finnhubClient.quote(
                    RangeQuoteRequest(
                        request.resolution,
                        seriesStartTime,
                        seriesEndTime,
                        request.parameterCode
                    )
                ) + finnhubClient.quote(
                    RangeQuoteRequest(
                        request.resolution,
                        seriesEndTime,
                        request.endTime,
                        request.parameterCode
                    )
                )
            } else {
                finnhubClient.quote(request)
            }
            seriesToSave += priceMap(prices)
            series += seriesToSave
        }
        val prices = distinct(series)
        val pricesToSave = distinct(seriesToSave)
        if (pricesToSave.isNotEmpty()) {
            log.info("Saving new prices [${pricesToSave.size}] ...")
            influxPriceClient.save(pricesToSave)
        }
        return prices
    }

    private fun distinct(series: Map<LocalDate, CandleVPrice>) = series.values.sortedBy { it.quoteDate }.toList()

    private fun priceMap(prices: List<CandleVPrice>): MutableMap<LocalDate, CandleVPrice> {
        return prices.filterNot { it.value == 0.0 }.associateBy { it.quoteDate }.toMutableMap()
    }

    private fun toRequest(request: RangeQuoteRequest, series: Map<LocalDate, CandleVPrice>): Boolean {
        val unit = max(1L, request.resolution.asDuration().toDays())
        val requestedDays = ChronoUnit.DAYS.between(request.startTime, request.endTime) / unit
        val minDays = (requestedDays * threshold).toInt()
        return series.isEmpty() || series.size < minDays
    }
}