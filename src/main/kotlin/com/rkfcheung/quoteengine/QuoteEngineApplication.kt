package com.rkfcheung.quoteengine

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class QuoteEngineApplication

fun main(args: Array<String>) {
	runApplication<QuoteEngineApplication>(*args)
}