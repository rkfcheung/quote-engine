package com.rkfcheung.quoteengine.config

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding

@ConstructorBinding
@ConfigurationProperties("connector.finnhub")
data class FinnhubProperties(val url: String, val token: String)
