package com.rkfcheung.quoteengine.config

import com.rkfcheung.quoteengine.client.FinnhubClient
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
@EnableConfigurationProperties(FinnhubProperties::class)
class FinnhubConfig() {
    @Autowired
    private lateinit var properties: FinnhubProperties

    @Bean(RestConstants.FINNHUB_CLIENT)
    fun finnhubClient() = FinnhubClient(properties)
}