package com.rkfcheung.quoteengine.config

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding

@ConstructorBinding
@ConfigurationProperties("connector.quandl")
data class QuandlProperties(val apiKey: String)
