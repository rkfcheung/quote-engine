package com.rkfcheung.quoteengine.config

object RestConstants {
    const val API_BASE = "/api/v1"
    const val API_QUOTE_BASE = "$API_BASE/quote"
    const val FINNHUB_CLIENT = "finnhub-client"
}