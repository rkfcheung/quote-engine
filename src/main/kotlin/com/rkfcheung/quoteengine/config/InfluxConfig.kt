package com.rkfcheung.quoteengine.config

import com.influxdb.client.InfluxDBClient
import com.influxdb.client.InfluxDBClientFactory
import com.influxdb.client.InfluxDBClientOptions
import com.influxdb.client.kotlin.InfluxDBClientKotlin
import com.influxdb.client.kotlin.InfluxDBClientKotlinFactory
import com.rkfcheung.quoteengine.client.InfluxPriceClient
import com.rkfcheung.quoteengine.model.definition.MeasurementBasics
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
@EnableConfigurationProperties(InfluxProperties::class)
class InfluxConfig {
    @Autowired
    private lateinit var properties: InfluxProperties

    @Bean(name = [MeasurementBasics.BUCKET_NAME])
    fun bucket(): String = properties.bucket

    @Bean
    fun influxClient(): InfluxDBClient = InfluxDBClientFactory.create(options())

    @Bean
    fun influxPriceClient(): InfluxPriceClient = InfluxPriceClient(influxClient(), properties)

    @Bean
    fun influxQueryClient(): InfluxDBClientKotlin = InfluxDBClientKotlinFactory.create(options())

    private fun options() = InfluxDBClientOptions.builder()
        .url(properties.url)
        .org(properties.org)
        .bucket(properties.bucket)
        .authenticateToken(properties.token.toCharArray())
        .build()
}