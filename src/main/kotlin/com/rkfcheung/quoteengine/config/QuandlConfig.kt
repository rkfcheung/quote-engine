package com.rkfcheung.quoteengine.config

import com.jimmoores.quandl.SessionOptions
import com.jimmoores.quandl.classic.ClassicQuandlSession
import com.rkfcheung.quoteengine.util.QuandlUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
@EnableConfigurationProperties(QuandlProperties::class)
class QuandlConfig {
    @Autowired
    private lateinit var properties: QuandlProperties

    @Bean
    fun quandlSession(): ClassicQuandlSession {
        val apiKey = properties.apiKey.replace(QuandlUtil.API_KEY, "")
        val options = if (apiKey.isEmpty()) {
            SessionOptions.Builder.withoutAuthToken()
        } else {
            SessionOptions.Builder.withAuthToken(properties.apiKey)
        }.build()
        return ClassicQuandlSession.create(options)
    }
}