package com.rkfcheung.quoteengine.config

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding

@ConstructorBinding
@ConfigurationProperties("connector.influx2")
data class InfluxProperties(val url: String, val org: String, val bucket: String, val token: String)
