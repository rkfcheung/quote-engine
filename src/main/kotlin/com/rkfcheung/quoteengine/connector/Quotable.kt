package com.rkfcheung.quoteengine.connector

import com.opengamma.strata.data.ObservableSource
import com.opengamma.strata.product.ProductType
import com.rkfcheung.quoteengine.model.price.Price
import com.rkfcheung.quoteengine.model.quote.QuoteRequest

interface Quotable<R : QuoteRequest, P : Price> {
    val parameterSource: ObservableSource
    val productType: ProductType

    fun quote(request: R): List<P>
}