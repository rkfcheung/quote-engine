package com.rkfcheung.quoteengine.connector

import com.rkfcheung.quoteengine.model.entity.AssetClass
import com.rkfcheung.quoteengine.model.entity.TickerEntity
import com.rkfcheung.quoteengine.model.entity.TickerViewId
import com.rkfcheung.quoteengine.model.entity.WithTicker
import com.rkfcheung.quoteengine.model.ticker.Ticker
import com.rkfcheung.quoteengine.repository.*
import com.rkfcheung.quoteengine.util.TickerUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.cache.annotation.Cacheable
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Component

@Component
class TickerEntityConnector {
    @Autowired
    private lateinit var assetClassRepository: AssetClassRepository

    @Autowired
    private lateinit var bloombergTickerRepository: BloombergTickerRepository

    @Autowired
    private lateinit var exchangeCentreRepository: ExchangeCentreRepository

    @Autowired
    private lateinit var hkExSecurityRepository: HkExSecurityRepository

    @Autowired
    private lateinit var tickerEntityRepository: TickerEntityRepository

    @Autowired
    private lateinit var tickerViewRepository: TickerViewRepository

    @Cacheable
    fun get(code: String): TickerEntity =
        TickerUtil.pretty(findWithTicker(code)?.asTickerEntity() ?: TickerEntity(code, code))

    @Cacheable
    fun getTicker(code: String): Ticker? = findWithTicker(code)?.asTicker() ?: TickerUtil.toTickerOrNull(code)

    @Cacheable
    fun getAssetClasses(): List<AssetClass> = assetClassRepository.findAll().toList()

    @Cacheable
    private fun findWithTicker(code: String): WithTicker? {
        val entities = (tickerViewRepository.findAllById(mapWithAssetClass(code)).filter { it.active() } +
                hkExSecurityRepository.findByIdOrNull(code) +
                exchangeCentreRepository.findByCode(code) +
                bloombergTickerRepository.findByCode(code) + bloombergTickerRepository.findByIdOrNull(code) +
                tickerEntityRepository.findByIdOrNull(code)).filterNotNull()
        val measurement = entities.firstOrNull()?.asTicker()?.asTickerMeasurement() ?: return null
        entities.forEach {
            measurement.update(it.asTicker())
        }
        return measurement
    }

    private fun mapWithAssetClass(code: String) = getAssetClasses().map { TickerViewId(code, it.id) }
}