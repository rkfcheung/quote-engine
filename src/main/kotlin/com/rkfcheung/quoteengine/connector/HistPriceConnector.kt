package com.rkfcheung.quoteengine.connector

import com.opengamma.strata.data.ObservableSource
import com.opengamma.strata.product.ProductType
import com.rkfcheung.quoteengine.model.definition.ParameterSource
import com.rkfcheung.quoteengine.model.definition.Resolution
import com.rkfcheung.quoteengine.model.price.CandleVPrice
import com.rkfcheung.quoteengine.model.quote.RangeQuoteRequest
import com.rkfcheung.quoteengine.repository.HistPriceRepository
import com.rkfcheung.quoteengine.util.DateTimeUtil
import com.rkfcheung.quoteengine.util.DateTimeUtil.asZonedDateTime
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.cache.annotation.Cacheable
import org.springframework.stereotype.Component
import java.time.LocalDate

@Component
class HistPriceConnector : RangeQuoteConnector<RangeQuoteRequest, CandleVPrice>() {
    override val parameterSource: ObservableSource = ParameterSource.MYSQL
    override val productType: ProductType = ProductType.OTHER
    private val since = LocalDate.of(1871, 1, 1).asZonedDateTime()

    @Autowired
    private lateinit var histPriceRepository: HistPriceRepository

    @Autowired
    private lateinit var tickerEntityConnector: TickerEntityConnector

    @Cacheable
    override fun quote(request: RangeQuoteRequest): List<CandleVPrice> {
        val code = request.parameterCode.yahooCode()
        val startDate = DateTimeUtil.max(request.startTime, since).toLocalDate()
        val endDate = DateTimeUtil.max(request.endTime, since).toLocalDate()
        val prices = histPriceRepository.findByCodeAndQuoteDateBetween(code, startDate, endDate)
        if (prices.isEmpty()) {
            return emptyList()
        }
        val tickerEntity = tickerEntityConnector.get(code)
        return prices.map { it.asCandleVPrice(Resolution.DAILY, tickerEntity) }
    }
}