package com.rkfcheung.quoteengine.connector

import com.opengamma.strata.data.ObservableSource
import com.rkfcheung.quoteengine.model.definition.ParameterSource
import com.rkfcheung.quoteengine.model.price.Price
import com.rkfcheung.quoteengine.model.quote.QuandlQuoteRequest

abstract class QuandlConnector<P : Price> : RangeQuoteConnector<QuandlQuoteRequest, P>() {
    override val parameterSource: ObservableSource = ParameterSource.QUANDL
}