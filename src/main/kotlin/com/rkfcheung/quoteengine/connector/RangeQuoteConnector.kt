package com.rkfcheung.quoteengine.connector

import com.rkfcheung.quoteengine.model.price.Price
import com.rkfcheung.quoteengine.model.quote.RangeQuoteRequest
import org.slf4j.Logger
import org.slf4j.LoggerFactory

abstract class RangeQuoteConnector<R : RangeQuoteRequest, P : Price> : Quotable<R, P> {
    protected val log: Logger = LoggerFactory.getLogger(this.javaClass)
}