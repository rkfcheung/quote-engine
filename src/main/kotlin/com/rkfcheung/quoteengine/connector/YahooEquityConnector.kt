package com.rkfcheung.quoteengine.connector

import com.opengamma.strata.basics.currency.Currency
import com.opengamma.strata.product.ProductType
import com.rkfcheung.quoteengine.model.price.CandleVPrice
import com.rkfcheung.quoteengine.model.quote.YahooQuoteRequest
import com.rkfcheung.quoteengine.util.YahooUtil.asCandleVPrice
import org.springframework.cache.annotation.Cacheable
import org.springframework.stereotype.Component
import java.io.IOException

@Component
class YahooEquityConnector : YahooConnector<CandleVPrice>() {
    override val productType: ProductType = ProductType.SECURITY

    @Cacheable
    override fun quote(request: YahooQuoteRequest): List<CandleVPrice> {
        val code = request.parameterCode
        return try {
            val stock = request.asStock()
            stock.history.map { it.asCandleVPrice(request.resolution, code, Currency.of(stock.currency)) }
                .filterNot { it.volume == 0.0 && it.open == it.high && it.high == it.low && it.low == it.close }
        } catch (e: IOException) {
            log.warn("Failed to get $request: $e")
            emptyList()
        }
    }
}