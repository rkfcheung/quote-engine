package com.rkfcheung.quoteengine.connector

import com.jimmoores.quandl.Frequency
import com.jimmoores.quandl.classic.ClassicQuandlSession
import com.opengamma.strata.basics.currency.Currency
import com.opengamma.strata.product.ProductType
import com.rkfcheung.quoteengine.model.definition.Resolution
import com.rkfcheung.quoteengine.model.price.CandleVPrice
import com.rkfcheung.quoteengine.model.quote.QuandlQuoteRequest
import com.rkfcheung.quoteengine.util.QuandlUtil.asPrices
import com.rkfcheung.quoteengine.util.QuandlUtil.asResolution
import org.json.JSONObject
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.cache.annotation.Cacheable
import org.springframework.stereotype.Component

@Component
class QuandlEquityConnector : QuandlConnector<CandleVPrice>() {
    @Autowired
    private lateinit var quandlSession: ClassicQuandlSession

    override val productType: ProductType = ProductType.SECURITY

    @Cacheable
    override fun quote(request: QuandlQuoteRequest): List<CandleVPrice> {
        val code = request.parameterCode
        val metaData = getMetaData(request)
        val resolution = getResolution(metaData)
        val currency = getCurrency(metaData)
        return quandlSession.getDataSet(request.asDataSetRequest()).asPrices(resolution, code, currency)
    }

    private fun getCurrency(metaData: JSONObject): Currency {
        val description = metaData.getString("description")
        if (description.contains("Currency:")) {
            val value = description.split(":").last().trim()
            if (value.length == 3) {
                return Currency.of(value.toUpperCase())
            }
        }
        return Currency.XXX
    }

    private fun getResolution(metaData: JSONObject): Resolution {
        val frequency = metaData.getString("frequency").toUpperCase()
        return Frequency.valueOf(frequency).asResolution()
    }

    private fun getMetaData(request: QuandlQuoteRequest): JSONObject {
        val metaData = quandlSession.getMetaData(request.asMetaDataRequest())
        return metaData.rawJSON.get("dataset") as JSONObject
    }
}