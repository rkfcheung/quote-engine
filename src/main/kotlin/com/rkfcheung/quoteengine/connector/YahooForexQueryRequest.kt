package com.rkfcheung.quoteengine.connector

import com.fasterxml.jackson.databind.JsonNode
import com.rkfcheung.quoteengine.model.definition.ParameterSource
import com.rkfcheung.quoteengine.model.definition.Resolution
import com.rkfcheung.quoteengine.model.price.ForexPrice
import com.rkfcheung.quoteengine.model.ticker.ForexTicker
import yahoofinance.Utils
import yahoofinance.quotes.query1v7.QuotesRequest
import java.time.ZonedDateTime

class YahooForexQueryRequest(private val code: ForexTicker) : QuotesRequest<ForexPrice>(code.yahooCode()) {
    override fun parseJson(node: JsonNode): ForexPrice {
        val price = Utils.getDouble(node["regularMarketPrice"].asText())
        val bid = node["bid"].asText()?.toDoubleOrNull() ?: price
        val ask = node["ask"].asText()?.toDoubleOrNull() ?: price
        val open = node["regularMarketOpen"].asText()?.toDoubleOrNull() ?: price
        val high = node["regularMarketDayHigh"].asText()?.toDoubleOrNull() ?: ask
        val low = node["regularMarketDayLow"].asText()?.toDoubleOrNull() ?: bid

        return ForexPrice(
            Resolution.MINUTE, ParameterSource.YAHOO, ZonedDateTime.now(), code, bid, ask
        ).apply {
            this.open = open
            this.high = high
            this.low = low
            this.close = price
        }
    }
}