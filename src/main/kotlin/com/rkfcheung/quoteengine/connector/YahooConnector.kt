package com.rkfcheung.quoteengine.connector

import com.opengamma.strata.data.ObservableSource
import com.rkfcheung.quoteengine.model.definition.ParameterSource
import com.rkfcheung.quoteengine.model.price.Price
import com.rkfcheung.quoteengine.model.quote.YahooQuoteRequest

abstract class YahooConnector<P : Price> : RangeQuoteConnector<YahooQuoteRequest, P>() {
    override val parameterSource: ObservableSource = ParameterSource.YAHOO
}