package com.rkfcheung.quoteengine.connector

import com.opengamma.strata.product.ProductType
import com.rkfcheung.quoteengine.model.price.ForexPrice
import com.rkfcheung.quoteengine.model.quote.YahooQuoteRequest
import com.rkfcheung.quoteengine.model.ticker.ForexTicker
import org.springframework.stereotype.Component

@Component
class YahooForexConnector : YahooConnector<ForexPrice>() {
    override val productType: ProductType = ProductType.FX_SINGLE

    fun quote(code: ForexTicker): ForexPrice {
        return YahooForexQueryRequest(code).singleResult
    }

    override fun quote(request: YahooQuoteRequest): List<ForexPrice> {
        return listOf(quote(request.parameterCode as ForexTicker))
    }
}