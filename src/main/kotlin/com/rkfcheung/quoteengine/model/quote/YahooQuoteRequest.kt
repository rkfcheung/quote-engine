package com.rkfcheung.quoteengine.model.quote

import com.rkfcheung.quoteengine.model.definition.ParameterSource
import com.rkfcheung.quoteengine.model.definition.Resolution
import com.rkfcheung.quoteengine.model.ticker.Ticker
import com.rkfcheung.quoteengine.util.DateTimeUtil
import com.rkfcheung.quoteengine.util.DateTimeUtil.asCalendar
import com.rkfcheung.quoteengine.util.DateTimeUtil.asZonedDateTime
import yahoofinance.Stock
import yahoofinance.YahooFinance
import java.time.LocalDate
import java.time.ZonedDateTime

class YahooQuoteRequest(
    override val resolution: Resolution,
    override val startTime: ZonedDateTime,
    override val endTime: ZonedDateTime,
    override val parameterCode: Ticker
) : RangeQuoteRequest(resolution, startTime, endTime, parameterCode, ParameterSource.YAHOO) {
    private val since = LocalDate.of(2000, 1, 1).asZonedDateTime()

    constructor(request: RangeQuoteRequest) : this(
        request.resolution,
        request.startTime,
        request.endTime,
        request.parameterCode
    )

    fun asStock(): Stock {
        val start = DateTimeUtil.max(startTime, since)
        val stop = DateTimeUtil.max(endTime, since)
        return YahooFinance.get(
            parameterCode.yahooCode(),
            start.asCalendar(),
            stop.asCalendar(),
            resolution.asInterval()
        )
    }
}