package com.rkfcheung.quoteengine.model.quote

import com.jimmoores.quandl.DataSetRequest
import com.jimmoores.quandl.MetaDataRequest
import com.jimmoores.quandl.SortOrder
import com.rkfcheung.quoteengine.model.definition.ParameterSource
import com.rkfcheung.quoteengine.model.definition.Resolution
import com.rkfcheung.quoteengine.model.ticker.EquityTicker
import java.time.ZonedDateTime

data class QuandlQuoteRequest(
    override val resolution: Resolution,
    override val startTime: ZonedDateTime,
    override val endTime: ZonedDateTime,
    override val parameterCode: EquityTicker
) : RangeQuoteRequest(resolution, startTime, endTime, parameterCode, ParameterSource.QUANDL) {
    fun asDataSetRequest(): DataSetRequest {
        return DataSetRequest.Builder.of(parameterCode.quandlCode())
            .withFrequency(resolution.asFrequency())
            .withStartDate(startTime.toLocalDate())
            .withEndDate(endTime.toLocalDate())
            .withSortOrder(SortOrder.DESCENDING)
            .build()
    }

    fun asMetaDataRequest(): MetaDataRequest = MetaDataRequest.of(parameterCode.quandlCode())
}