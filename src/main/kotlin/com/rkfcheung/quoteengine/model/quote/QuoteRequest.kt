package com.rkfcheung.quoteengine.model.quote

import com.opengamma.strata.data.ObservableSource
import com.rkfcheung.quoteengine.model.definition.Resolution
import com.rkfcheung.quoteengine.model.ticker.Ticker
import java.time.ZonedDateTime

abstract class QuoteRequest(
    open val resolution: Resolution,
    open val timestamp: ZonedDateTime,
    open val parameterCode: Ticker,
    open val parameterSource: ObservableSource? = null
)