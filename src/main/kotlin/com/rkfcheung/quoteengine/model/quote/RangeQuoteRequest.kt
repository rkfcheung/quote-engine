package com.rkfcheung.quoteengine.model.quote

import com.opengamma.strata.data.ObservableSource
import com.rkfcheung.quoteengine.model.definition.Resolution
import com.rkfcheung.quoteengine.model.ticker.Ticker
import java.time.ZonedDateTime

open class RangeQuoteRequest(
    override val resolution: Resolution,
    open val startTime: ZonedDateTime,
    open val endTime: ZonedDateTime,
    override val parameterCode: Ticker,
    override val parameterSource: ObservableSource? = null
) : QuoteRequest(resolution, endTime, parameterCode, parameterSource) {
    override fun toString(): String {
        return "RangeQuoteRequest(resolution=$resolution, startTime=$startTime, endTime=$endTime, parameterCode=$parameterCode, parameterSource=$parameterSource)"
    }
}
