package com.rkfcheung.quoteengine.model.entity

import com.rkfcheung.quoteengine.model.ticker.Ticker

interface WithTicker {
    val code: String

    fun asTicker(): Ticker

    fun asTickerEntity(): TickerEntity
}