package com.rkfcheung.quoteengine.model.entity

import java.io.Serializable

data class TickerViewId(val code: String = "", val assetClassId: Int = 0): Serializable
