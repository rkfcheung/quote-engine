package com.rkfcheung.quoteengine.model.entity

import java.io.Serializable
import java.time.LocalDate

data class HistPriceId(val code: String = "", val quoteDate: LocalDate = LocalDate.now()): Serializable
