package com.rkfcheung.quoteengine.model.entity

import com.opengamma.strata.basics.currency.Currency
import com.opengamma.strata.basics.location.Country
import com.opengamma.strata.product.ProductType
import com.opengamma.strata.product.common.ExchangeIds
import com.rkfcheung.quoteengine.model.definition.Entities
import com.rkfcheung.quoteengine.model.ticker.Ticker
import com.rkfcheung.quoteengine.util.TickerUtil
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = Entities.HkExSecurity)
data class HkExSecurity(
    @Id @Column(name = "ticker") override val code: String,
    val name: String,
    @Column(name = "lotsize") val lotSize: Int = 0,
    val board: String? = null
) : WithTicker {
    override fun asTicker(): Ticker = (TickerUtil.toEquityTickerOrNull(code) ?: Ticker(productType(), code)).apply {
        add(this@HkExSecurity.currency())
        add(ExchangeIds.XHKG)
        addLotSize(lotSize)
    }

    override fun asTickerEntity(): TickerEntity = TickerEntity(code, name, currency().code, market())

    fun currency(): Currency = TickerUtil.toCurrency(code) ?: Currency.HKD

    fun market(): String = Country.HK.code

    fun productType(): ProductType = TickerUtil.asProductType(code, ProductType.SECURITY)
}