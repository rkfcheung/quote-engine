package com.rkfcheung.quoteengine.model.entity.finnhub

import com.opengamma.strata.basics.StandardSchemes
import com.opengamma.strata.basics.currency.Currency
import com.opengamma.strata.product.ProductType
import com.opengamma.strata.product.common.ExchangeId
import com.rkfcheung.quoteengine.model.definition.ExtraTypedConstants
import com.rkfcheung.quoteengine.model.entity.TickerEntity
import com.rkfcheung.quoteengine.model.entity.WithTicker
import com.rkfcheung.quoteengine.model.ticker.Ticker
import com.rkfcheung.quoteengine.util.MarketMapper
import com.rkfcheung.quoteengine.util.TickerUtil
import com.rkfcheung.quoteengine.util.YahooUtil

data class StockSymbol(
    val symbol: String,
    val displaySymbol: String,
    val description: String,
    val figi: String,
    val mic: String,
    val currency: String,
    val type: String,
) : WithTicker {
    override val code = YahooUtil.toCode(symbol, micCode())

    override fun asTicker(): Ticker {
        return Ticker(productType(), code).apply {
            add(true)
            add(this@StockSymbol.currency())
            add(this@StockSymbol.micCode())
            add(StandardSchemes.BBG_SCHEME, this@StockSymbol.bloombergCode())
            add(StandardSchemes.FIGI_SCHEME, figi)
            add(ExtraTypedConstants.FINNHUB_TICKER_SCHEME, symbol)
            addName(description)
        }
    }

    override fun asTickerEntity(): TickerEntity {
        return TickerEntity(code, description, currency, MarketMapper.toMarket(micCode()) ?: MarketMapper.XX)
    }

    fun bloombergCode(): String = YahooUtil.toBloombergCode(symbol, micCode()) ?: symbol.replace(".", ":")

    fun currency(): Currency = Currency.of(currency)

    fun figiCode(): String = figi

    fun micCode(): ExchangeId = ExchangeId.of(mic)

    fun productType(): ProductType = TickerUtil.toProductTypeOrNull(type) ?: ProductType.of(type.replace(" ", ""))
}