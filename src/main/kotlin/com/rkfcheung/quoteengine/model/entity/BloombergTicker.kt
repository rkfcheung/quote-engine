package com.rkfcheung.quoteengine.model.entity

import com.opengamma.strata.basics.StandardId
import com.opengamma.strata.basics.StandardSchemes
import com.opengamma.strata.basics.currency.Currency
import com.opengamma.strata.product.ProductType
import com.opengamma.strata.product.common.ExchangeId
import com.rkfcheung.quoteengine.model.definition.Entities
import com.rkfcheung.quoteengine.model.ticker.EquityTicker
import com.rkfcheung.quoteengine.model.ticker.Ticker
import com.rkfcheung.quoteengine.util.MarketMapper
import com.rkfcheung.quoteengine.util.TickerUtil
import java.time.LocalDateTime
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = Entities.BloombergTicker)
data class BloombergTicker(
    @Id @Column(name = "btk_id") val id: String,
    @Column(name = "btk_tickerid") override val code: String,
    @Column(name = "btk_figi") val figi: String,
    @Column(name = "btk_issuedcurrency") val currency: String,
    @Column(name = "btk_name") val name: String,
    @Column(name = "btk_primaryexchange") val exchange: String,
    @Column(name = "btk_bicsindustry") val industry: String?,
    @Column(name = "btk_bicssector") val sector: String,
    @Column(name = "btk_bicssubindustry") val subIndustry: String?,
    @Column(name = "btk_marketstatus") val marketStatus: String,
    @Column(name = "btk_securitytype") val securityType: String = ProductType.OTHER.name,
    @Column(name = "btk_lastupdatetime") val lastUpdateTime: LocalDateTime
) : WithTicker {
    override fun asTicker(): Ticker = when (val type = productType()) {
        ProductType.FX_SINGLE -> TickerUtil.asForexTicker(code)
        ProductType.SECURITY -> micCode()?.let { mic -> EquityTicker(mic, code) } ?: Ticker(type, code)
        else -> Ticker(type, code)
    }.apply {
        add(this@BloombergTicker.active())
        add(this@BloombergTicker.standardId())
        add(StandardSchemes.FIGI_SCHEME, figi)
        this@BloombergTicker.micCode()?.let { add(it) }
        add(Currency.of(currency))
        addName(name)
    }

    override fun asTickerEntity(): TickerEntity = TickerEntity(code, name, currency, market())

    fun active(): Boolean = marketStatus == "ACTV"

    fun market(): String = MarketMapper.toMarket(code, toExchangeId()) ?: MarketMapper.XX

    fun micCode(): ExchangeId? = toExchangeId() ?: MarketMapper.toExchangeIdOrNull(market())

    fun productType(): ProductType = TickerUtil.toProductTypeOrNull(securityType) ?: ProductType.OTHER

    fun standardId(): StandardId = StandardId.of(StandardSchemes.BBG_SCHEME, id)

    private fun toExchangeId() = MarketMapper.toExchangeIdOrNull(exchange)
}
