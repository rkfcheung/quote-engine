package com.rkfcheung.quoteengine.model.entity

import com.opengamma.strata.basics.StandardSchemes
import com.opengamma.strata.basics.currency.Currency
import com.opengamma.strata.product.ProductType
import com.rkfcheung.quoteengine.model.definition.Entities
import com.rkfcheung.quoteengine.model.definition.ExtraTypedConstants
import com.rkfcheung.quoteengine.model.ticker.Ticker
import com.rkfcheung.quoteengine.util.MarketMapper
import com.rkfcheung.quoteengine.util.TickerUtil
import javax.persistence.*

@Entity
@Table(name = Entities.ExchangeCentre)
data class ExchangeCentre(
    @Id val id: Int,
    val name: String,
    val symbol: String,
    @Column(name = "indexcode") override val code: String,
    val country: Int,
    @Column(name = "idxchg") val idxChg: Double = 0.0,
    @Column(name = "bbcode") val bloombergCode: String? = null,
    @Column(name = "sgcode") val sgCode: String? = null,
    val seq: Int = 9999
) : WithTicker {
    override fun asTicker(): Ticker = Ticker(productType(), code).apply {
        bloombergCode?.let { add(StandardSchemes.BBG_SCHEME, it) }
        addName(name)
    }

    override fun asTickerEntity(): TickerEntity = TickerUtil.pretty(asTickerEntity(Currency.XXX, MarketMapper.XX))

    fun asTickerEntity(currency: Currency, market: String): TickerEntity =
        TickerEntity(code, name, currency.code, market)

    fun productType(): ProductType = ExtraTypedConstants.INDEX
}
