package com.rkfcheung.quoteengine.model.entity

import com.opengamma.strata.product.ProductType
import com.rkfcheung.quoteengine.model.definition.Entities
import com.rkfcheung.quoteengine.util.TickerUtil
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = Entities.AssetClass)
data class AssetClass(
    @Id @Column(name = "acs_id") val id: Int,
    @Column(name = "acs_name") val name: String,
    @Column(name = "acs_marketable") val marketable: Boolean
) {
    fun asProductType(): ProductType = TickerUtil.asProductType(this)
}