package com.rkfcheung.quoteengine.model.entity

import com.opengamma.strata.basics.currency.Currency
import com.opengamma.strata.collect.timeseries.LocalDateDoublePoint
import com.rkfcheung.quoteengine.model.definition.Entities
import com.rkfcheung.quoteengine.model.definition.ParameterSource
import com.rkfcheung.quoteengine.model.definition.Resolution
import com.rkfcheung.quoteengine.model.price.CandleVPrice
import com.rkfcheung.quoteengine.util.UnitUtil
import java.time.LocalDate
import javax.persistence.*

@Entity
@Table(name = Entities.HistPrice)
@IdClass(HistPriceId::class)
data class HistPrice(
    @Id @Column(name = "ticker") val code: String,
    @Id @Column(name = "quotedate") val quoteDate: LocalDate,
    val open: Double = 0.0,
    val high: Double = 0.0,
    val low: Double = 0.0,
    val close: Double = 0.0,
    val volume: Long = 0L,
    @Column(name = "adjclose") val adjClose: Double = 0.0
) {
    fun asCandleVPrice(
        resolution: Resolution = Resolution.DAILY,
        tickerEntity: TickerEntity = TickerEntity(code, code)
    ): CandleVPrice {
        return CandleVPrice(
            resolution,
            ParameterSource.MYSQL,
            quoteDate,
            tickerEntity.asTicker(),
            open,
            high,
            low,
            close,
            UnitUtil.toDouble(volume),
            Currency.of(tickerEntity.currency)
        ).apply {
            this.adjClose = this@HistPrice.adjClose
        }
    }

    fun asLocalDateDoublePoint(): LocalDateDoublePoint = LocalDateDoublePoint.of(quoteDate, adjClose)
}