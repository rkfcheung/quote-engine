package com.rkfcheung.quoteengine.model.entity

import com.opengamma.strata.basics.StandardSchemes
import com.opengamma.strata.product.ProductType
import com.opengamma.strata.product.common.ExchangeIds
import com.rkfcheung.quoteengine.model.definition.Entities
import com.rkfcheung.quoteengine.model.ticker.Ticker
import com.rkfcheung.quoteengine.util.MarketMapper
import com.rkfcheung.quoteengine.util.TickerUtil
import javax.persistence.*

@Entity
@Table(name = Entities.TickerView)
@IdClass(TickerViewId::class)
data class TickerView(
    @Id @Column(name = "tkv_tickerid") override val code: String,
    @Column(name = "tkv_name") val name: String,
    @Column(name = "tkv_curr") val currency: String,
    @Column(name = "tkv_market") val market: String,
    @Id @Column(name = "tkv_assetclass") val assetClassId: Int
) : WithTicker {
    override fun asTicker(): Ticker = TickerUtil.asTicker(code, productType()).apply {
        add(this@TickerView.active())
        add(TickerUtil.asCurrency(currency))
        MarketMapper.toExchangeIdOrNull(market)?.let { add(it) }
        if (MarketMapper.isHKG(market)) {
            add(ExchangeIds.XHKG)
            bloombergCode()?.let { add(StandardSchemes.BBG_SCHEME, it) }
        }
        addName(name)
    }

    override fun asTickerEntity(): TickerEntity = TickerEntity(code, name, currency, market)

    fun active(): Boolean = code != name && name != "N/A"

    fun productType(): ProductType = AssetClass(assetClassId, ProductType.OTHER.name, false).asProductType()
}