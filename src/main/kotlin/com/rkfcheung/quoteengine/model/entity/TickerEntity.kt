package com.rkfcheung.quoteengine.model.entity

import com.opengamma.strata.basics.currency.Currency
import com.rkfcheung.quoteengine.model.definition.Entities
import com.rkfcheung.quoteengine.model.ticker.Ticker
import com.rkfcheung.quoteengine.util.MarketMapper
import com.rkfcheung.quoteengine.util.TickerUtil
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = Entities.TickerEntity)
data class TickerEntity(
    @Id @Column(name = "ticker") override val code: String,
    val name: String,
    val currency: String = Currency.XXX.code,
    val market: String = MarketMapper.XX,
    @Column(name = "lotsize") val lotSize: Int = 0
) : WithTicker {
    override fun asTicker(): Ticker = TickerUtil.asTicker(code).apply {
        add(TickerUtil.asCurrency(currency))
        MarketMapper.toExchangeIdOrNull(market)?.let { add(it) }
        addLotSize(lotSize)
        addName(name)
    }

    override fun asTickerEntity(): TickerEntity = this

    fun isMissing(): Boolean =
        code == name && currency == Currency.XXX.code && market == MarketMapper.XX && lotSize == 0
}