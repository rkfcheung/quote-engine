package com.rkfcheung.quoteengine.model.entity.finnhub

import com.finnhub.api.models.Stock

data class StockSymbolResult(val count: Int, val result: List<Stock>)
