package com.rkfcheung.quoteengine.model.ticker

import com.fasterxml.jackson.annotation.JsonBackReference
import com.fasterxml.jackson.annotation.JsonIgnore
import com.opengamma.strata.basics.StandardSchemes
import com.opengamma.strata.basics.currency.Currency
import com.opengamma.strata.basics.currency.CurrencyPair
import com.opengamma.strata.product.ProductType
import com.rkfcheung.quoteengine.model.entity.TickerEntity
import com.rkfcheung.quoteengine.util.MarketMapper
import com.rkfcheung.quoteengine.util.TickerUtil
import com.rkfcheung.quoteengine.util.YahooUtil

data class ForexTicker(@JsonBackReference val base: Currency, @JsonBackReference val counter: Currency) :
    Ticker(ProductType.FX_SINGLE, YahooUtil.toCode(base, counter)) {

    constructor(base: String, counter: String) : this(Currency.of(base), Currency.of(counter))

    init {
        add(counter)
        add(StandardSchemes.BBG_SCHEME, "$base$counter${TickerUtil.CUR_TYPE}")
    }

    @JsonIgnore val pair: CurrencyPair = CurrencyPair.of(base, counter)

    override fun currency(): Currency = counter

    override fun yahooCode(): String = YahooUtil.toCode(base, counter)

    fun inverse(): ForexTicker = ForexTicker(counter, base)

    fun asTickerEntity(): TickerEntity {
        val currency = counter.code
        val market = MarketMapper.toCountryOrNull(currency)?.code ?: MarketMapper.XX
        return TickerEntity(yahooCode(), pair.toString(), currency, market, 1)
    }
}