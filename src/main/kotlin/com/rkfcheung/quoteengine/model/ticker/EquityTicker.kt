package com.rkfcheung.quoteengine.model.ticker

import com.opengamma.strata.product.ProductType
import com.opengamma.strata.product.common.ExchangeId
import com.rkfcheung.quoteengine.util.QuandlUtil
import com.rkfcheung.quoteengine.util.YahooUtil

data class EquityTicker(val mic: ExchangeId, override val code: String) : Ticker(ProductType.SECURITY, code) {

    init {
        add(mic)
    }

    override fun bloombergCode(): String? {
        return YahooUtil.toBloombergCode(code, mic) ?: super.bloombergCode()
    }

    override fun micCode(): ExchangeId = mic

    override fun yahooCode(): String = YahooUtil.toCode(code, mic)

    fun quandlCode(): String? = QuandlUtil.toCode(code, mic)
}