package com.rkfcheung.quoteengine.model.ticker.measurement

import com.influxdb.annotations.Column
import com.influxdb.annotations.Measurement
import com.influxdb.query.FluxRecord
import com.influxdb.query.FluxTable
import com.opengamma.strata.basics.StandardSchemes
import com.opengamma.strata.basics.currency.Currency
import com.opengamma.strata.product.ProductType
import com.opengamma.strata.product.common.ExchangeId
import com.rkfcheung.quoteengine.model.definition.Entities
import com.rkfcheung.quoteengine.model.definition.MeasurementBasics
import com.rkfcheung.quoteengine.model.entity.TickerEntity
import com.rkfcheung.quoteengine.model.entity.WithTicker
import com.rkfcheung.quoteengine.model.ticker.EquityTicker
import com.rkfcheung.quoteengine.model.ticker.Ticker
import com.rkfcheung.quoteengine.util.DateTimeUtil.asInstant
import com.rkfcheung.quoteengine.util.MarketMapper
import com.rkfcheung.quoteengine.util.TickerUtil
import java.time.Instant
import java.time.LocalDate

@Measurement(name = Entities.TickerEntity)
data class TickerMeasurement(
    @Column(tag = true) override var code: String = "",
    @Column(timestamp = true) var time: Instant = LocalDate.now().asInstant(),
    @Column var name: String? = null,
    @Column var bloombergCode: String? = null,
    @Column var figiCode: String? = null,
    @Column var micCode: String? = null,
    @Column var currency: String? = null,
    @Column var lotSize: Int? = null,
    @Column var productType: String? = null,
    @Column var active: Boolean? = null
) : WithTicker {
    override fun asTicker(): Ticker {
        val exchange = micCode()
        val type = productType?.let { ProductType.of(it) } ?: ProductType.OTHER
        return when (type) {
            ProductType.SECURITY -> exchange?.let { EquityTicker(exchange, code) } ?: Ticker(type, code)
            ProductType.FX_SINGLE -> TickerUtil.asForexTicker(code)
            else -> Ticker(type, code)
        }.apply {
            name?.let { addName(it) }
            bloombergCode?.let { add(StandardSchemes.BBG_SCHEME, it) }
            figiCode?.let { add(StandardSchemes.FIGI_SCHEME, it) }
            exchange?.let { add(it) }
            currency?.let { add(Currency.of(currency)) }
            lotSize?.let { addLotSize(it) }
            active?.let { add(it) }
        }
    }

    override fun asTickerEntity(): TickerEntity {
        val nameOrCode = name ?: code
        val market = MarketMapper.toMarket(code, micCode()) ?: MarketMapper.XX
        val entity = TickerEntity(code, nameOrCode, currency ?: Currency.XXX.code, market, lotSize ?: 0)
        return TickerUtil.pretty(entity)
    }

    fun micCode(): ExchangeId? = micCode?.let { ExchangeId.of(it) }

    fun update(measurement: TickerMeasurement): Boolean {
        if (update(measurement.asTicker())) {
            time = measurement.time
            return true
        }
        return false
    }

    fun update(tables: List<FluxTable>): Boolean {
        if (tables.isEmpty()) {
            return false
        }
        var updated = false
        tables.forEach { table ->
            table.records.forEach { record ->
                updated = update(record) || updated
            }
        }
        return updated
    }

    fun update(ticker: Ticker): Boolean {
        if (ticker.yahooCode() != code) {
            return false
        }
        if (ticker.name() != null && ticker.name() != code) {
            name = ticker.name()
        }
        if (bloombergCode == null) {
            bloombergCode = ticker.bloombergCode()
        }
        if (figiCode == null) {
            figiCode = ticker.figiCode()
        }
        if (micCode == null) {
            micCode = ticker.micCode().toString()
        }
        currency = ticker.currency()?.code ?: currency
        lotSize = ticker.lotSize() ?: lotSize
        if (ticker.type != ProductType.OTHER) {
            productType = ticker.type.name
        }
        active = ticker.active() ?: active
        return true
    }

    private fun update(record: FluxRecord): Boolean {
        if (record.getValueByKey(MeasurementBasics.CODE)?.toString() != code) {
            return false
        }
        record.time?.let { if (it.isAfter(time)) time = it }
        val value = record.value.toString()
        when (record.field) {
            MeasurementBasics.ACTIVE -> active = value.toBoolean()
            MeasurementBasics.BLOOMBERG_CODE -> bloombergCode = value
            MeasurementBasics.CURRENCY -> currency = value
            MeasurementBasics.FIGI_CODE -> figiCode = value
            MeasurementBasics.LOT_SIZE -> lotSize = value.toIntOrNull()
            MeasurementBasics.MIC_CODE -> micCode = value
            MeasurementBasics.NAME_FIELD -> name = value
            MeasurementBasics.PRODUCT_TYPE -> productType = value
        }
        return true
    }
}