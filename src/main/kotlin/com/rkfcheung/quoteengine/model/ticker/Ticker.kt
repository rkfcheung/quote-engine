package com.rkfcheung.quoteengine.model.ticker

import com.opengamma.strata.basics.StandardId
import com.opengamma.strata.basics.StandardSchemes
import com.opengamma.strata.basics.currency.Currency
import com.opengamma.strata.calc.ColumnName
import com.opengamma.strata.product.ProductType
import com.opengamma.strata.product.common.ExchangeId
import com.rkfcheung.quoteengine.model.definition.ExtraTypedConstants
import com.rkfcheung.quoteengine.model.definition.TickerAttribute
import com.rkfcheung.quoteengine.model.ticker.measurement.TickerMeasurement
import com.rkfcheung.quoteengine.util.YahooUtil

open class Ticker(val type: ProductType, open val code: String) {
    private val schemes = mutableMapOf<String, String>()
    private val info = mutableMapOf<TickerAttribute, Any>()

    open fun bloombergCode(): String? =
        schemes[StandardSchemes.BBG_SCHEME] ?: YahooUtil.toBloombergCode(yahooCode())

    open fun currency(): Currency? = info[TickerAttribute.CURRENCY] as? Currency

    open fun micCode(): ExchangeId? = info[TickerAttribute.MIC] as? ExchangeId

    open fun yahooCode(): String = code

    fun active(): Boolean? = info[TickerAttribute.ACTIVE] as? Boolean

    fun asMeasurementCode(): ColumnName = ColumnName.of(yahooCode())

    fun asTickerMeasurement(): TickerMeasurement =
        TickerMeasurement(asMeasurementCode().name).apply { update(this@Ticker) }

    fun description(): String =
        "${javaClass.simpleName}(type=$type, code=${asMeasurementCode()}, info=$info, schemes=$schemes)"

    fun figiCode(): String? = schemes[StandardSchemes.FIGI_SCHEME]

    fun finnhubCode(): String? = schemes[ExtraTypedConstants.FINNHUB_TICKER_SCHEME]

    fun lotSize(): Int? = info[TickerAttribute.LOT_SIZE] as? Int

    fun name(): String? = info[TickerAttribute.NAME]?.toString()

    fun standardId(): StandardId = StandardId.of(ExtraTypedConstants.YAHOO_TICKER_SCHEME, yahooCode())

    fun add(active: Boolean): Ticker {
        info[TickerAttribute.ACTIVE] = active
        return this
    }

    fun add(currency: Currency): Ticker {
        if (currency != Currency.XXX) {
            info[TickerAttribute.CURRENCY] = currency
        }
        return this
    }

    fun add(exchangeMic: ExchangeId): Ticker {
        info[TickerAttribute.MIC] = exchangeMic
        return this
    }

    fun add(id: StandardId): Ticker {
        schemes[id.scheme] = id.value
        return this
    }

    fun add(scheme: String, value: String): Ticker {
        schemes[scheme] = value
        return this
    }

    fun addLotSize(lotSize: Int): Ticker {
        if (lotSize > 0) {
            info[TickerAttribute.LOT_SIZE] = lotSize
        }
        return this
    }

    fun addName(name: String): Ticker {
        if (name != yahooCode()) {
            info[TickerAttribute.NAME] = name
        }
        return this
    }

    fun findId(scheme: String): StandardId? = schemes[scheme]?.let { StandardId.of(scheme, it) }

    fun ids(): List<StandardId> = schemes.mapNotNull { StandardId.of(it.key, it.value) }

    override fun toString(): String = description()
}