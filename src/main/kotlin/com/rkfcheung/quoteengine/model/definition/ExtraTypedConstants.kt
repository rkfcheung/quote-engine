package com.rkfcheung.quoteengine.model.definition

import com.opengamma.strata.product.ProductType

object ExtraTypedConstants {
    const val FINNHUB_TICKER_SCHEME = "Finnhub-Ticker"
    const val YAHOO_TICKER_SCHEME = "Yahoo-Ticker"
    val FUND: ProductType = ProductType.of("Fund")
    val INDEX: ProductType = ProductType.of("Index")
    val INSURANCE: ProductType = ProductType.of("Insurance")
    val PENSION_FUND: ProductType = ProductType.of("PensionFund")
    val SELF_FUND: ProductType = ProductType.of("SelfFund")
    val SELF_MARKET_INDEX: ProductType = ProductType.of("SelfMarketIndex")
    val SELF_ECONOMIC_INDEX: ProductType = ProductType.of("SelfEconomicIndex")
}