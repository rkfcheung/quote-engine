package com.rkfcheung.quoteengine.model.definition

object Entities {
    /*
    +-------------------------+--------+
    | Tables                  | Status |
    +-------------------------+--------+
    | acs_assetclass          | OK     |
    | ahp_archivehistprice    | SKIP   |
    | bkr_brokerage           |        |
    | bloomberg-realized      | SKIP   |
    | bloomberg-watchlist     | SKIP   |
    | bqv_basicquoteview      | SKIP   |
    | btk_bloombergticker     | OK     |
    | cashflow                |        |
    | cfw_cashflow            |        |
    | cps_composite           |        |
    | crz_watchlist           | SKIP   |
    | ctg_category            |        |
    | dfi_diffusionindex      |        |
    | div_dividend            |        |
    | dlt_delist              | SKIP   |
    | dts_datasource          |        |
    | ecm_exchangecodemapping |        |
    | efd                     | SKIP   |
    | exchangecentre          | OK     |
    | fct_fundchart           | SKIP   |
    | fundview                | SKIP   |
    | grp_group               | SKIP   |
    | hcm_hkcodemapping       |        |
    | hhg_histholding         |        |
    | histprice               | OK     |
    | hkexsecurity            | OK     |
    | hld_holiday             |        |
    | icf_investmentcashflow  |        |
    | ins_insurance           |        |
    | investment              |        |
    | itt_investment          |        |
    | ivr_investor            |        |
    | ixs_incexpstmt          |        |
    | ldv_lastdateview        | OK     |
    | mag_movingaverage       |        |
    | marketprice             | SKIP   |
    | mbt_marketbreadth       |        |
    | mhp_mpfhistprice        |        |
    | mpi_mpfinvestment       |        |
    | mpt_mpfticker           |        |
    | ohp_otherhistprice      |        |
    | opc_optionsclass        |        |
    | parameter               | SKIP   |
    | pfo_portfolio           |        |
    | portfolio               |        |
    | ptn_position            |        |
    | realizedgainloss        |        |
    | rgl_realizedgainloss    |        |
    | rhp_rchistprice         |        |
    | securityheader          | SKIP   |
    | spl_suspensionlist      |        |
    | spq_specialquote        |        |
    | sqv_specialquoteview    |        |
    | stocklist               | SKIP   |
    | stt_strategytest        |        |
    | summaryview             | SKIP   |
    | testfund                | SKIP   |
    | thp_tmphistprice        |        |
    | ticker                  | OK     |
    | tkv_tickerview          | OK     |
    | transaction             |        |
    | txn_transaction         |        |
    | vlc_velocity            |        |
    | warrant2                | SKIP   |
    | warrants                | SKIP   |
    | watchlist               | SKIP   |
    | wlt_watchlist           | SKIP   |
    | worldbase               | SKIP   |
    | xcs_exchcodeset         | SKIP   |
    +-------------------------+--------+
     */

    const val AssetClass = "acs_assetclass"
    const val BloombergTicker = "btk_bloombergticker"
    const val ExchangeCentre = "exchangecentre"
    const val HistPrice = "histprice"
    const val HkExSecurity = "hkexsecurity"
    const val MpfTicker = "mpt_mpfticker"
    const val TickerEntity = "ticker"
    const val TickerView = "tkv_tickerview"
}