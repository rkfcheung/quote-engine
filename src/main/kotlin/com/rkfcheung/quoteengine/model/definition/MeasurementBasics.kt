package com.rkfcheung.quoteengine.model.definition

import com.influxdb.client.domain.WritePrecision

object MeasurementBasics {
    const val BUCKET_NAME = "influx-bucket"
    const val PRICE = "price"
    const val CODE = "code"
    const val VALUE = "value"
    const val RESOLUTION = "resolution"
    const val ACTIVE = "active"
    const val BLOOMBERG_CODE = "bloombergCode"
    const val CURRENCY = "currency"
    const val FIGI_CODE = "figiCode"
    const val LOT_SIZE = "lotSize"
    const val MIC_CODE = "micCode"
    const val NAME_FIELD = "name"
    const val PRODUCT_TYPE = "productType"
    const val PARAMETER_SOURCE = "parameterSource"
    val WRITE_PRECISION = WritePrecision.S
    val ALL_PRICE_FIELDS: List<String> = PriceAttribute.values().map { it.name }
    val CANDLE_V_FIELDS: List<String> = listOf(
        PriceAttribute.OPEN,
        PriceAttribute.HIGH,
        PriceAttribute.LOW,
        PriceAttribute.CLOSE,
        PriceAttribute.ADJ_CLOSE,
        PriceAttribute.VOLUME
    ).map { it.name }
}