package com.rkfcheung.quoteengine.model.definition

import com.opengamma.strata.product.ProductType

enum class BloombergSecurityType {
    COMMON_STOCK,
    CROSS,
    CURRENCY,
    ETF,
    ETP,
    HEDGE_FUND,
    INDEX,
    MONEY_MARKET_FUND,
    OPEN_END_FUND,
    REIT,
    RIGHT,
    STAPLED_SECURITY;

    fun asProductType(): ProductType = when (this) {
        COMMON_STOCK, STAPLED_SECURITY -> ProductType.SECURITY
        CROSS, CURRENCY -> ProductType.FX_SINGLE
        INDEX -> ExtraTypedConstants.INDEX
        MONEY_MARKET_FUND, OPEN_END_FUND -> ExtraTypedConstants.FUND
        RIGHT -> ProductType.ETD_OPTION
        else -> ProductType.of(this.name)
    }
}