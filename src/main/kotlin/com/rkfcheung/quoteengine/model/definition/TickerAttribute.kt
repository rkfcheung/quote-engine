package com.rkfcheung.quoteengine.model.definition

enum class TickerAttribute {
    ACTIVE,
    CURRENCY,
    LOT_SIZE,
    MIC,
    NAME
}