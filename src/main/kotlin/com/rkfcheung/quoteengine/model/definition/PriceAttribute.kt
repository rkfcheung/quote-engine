package com.rkfcheung.quoteengine.model.definition

enum class PriceAttribute {
    BID,
    ASK,
    OPEN,
    HIGH,
    LOW,
    CLOSE,
    ADJ_CLOSE,
    VOLUME,
    TURNOVER
}