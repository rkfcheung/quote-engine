package com.rkfcheung.quoteengine.model.definition

import com.jimmoores.quandl.Frequency
import yahoofinance.histquotes.Interval
import java.time.Duration

enum class Resolution {
    NONE,
    TICK,
    MINUTE,
    HOURLY,
    DAILY,
    WEEKLY,
    MONTHLY,
    QUARTERLY,
    ANNUAL;

    fun asDuration(): Duration = when (this) {
        NONE -> Duration.ZERO
        TICK -> Duration.ofNanos(1L)
        MINUTE -> Duration.ofMinutes(1L)
        HOURLY -> Duration.ofHours(1L)
        DAILY -> Duration.ofDays(1L)
        WEEKLY -> Duration.ofDays(7L)
        MONTHLY -> Duration.ofDays(30L)
        QUARTERLY -> Duration.ofDays(120L)
        ANNUAL -> Duration.ofDays(365L)
    }

    fun asFrequency(): Frequency = when (this) {
        DAILY -> Frequency.DAILY
        WEEKLY -> Frequency.WEEKLY
        MONTHLY -> Frequency.MONTHLY
        QUARTERLY -> Frequency.QUARTERLY
        ANNUAL -> Frequency.ANNUAL
        else -> Frequency.NONE
    }

    fun asInterval(): Interval = when (this) {
        WEEKLY -> Interval.WEEKLY
        MONTHLY, QUARTERLY, ANNUAL -> Interval.MONTHLY
        else -> Interval.DAILY
    }
}