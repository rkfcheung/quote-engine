package com.rkfcheung.quoteengine.model.definition

import com.opengamma.strata.data.ObservableSource

object ParameterSource {
    val FINNHUB: ObservableSource = ObservableSource.of("FINNHUB")
    val MODEL: ObservableSource = ObservableSource.of("MODEL")
    val MYSQL: ObservableSource = ObservableSource.of("MYSQL")
    val QUANDL: ObservableSource = ObservableSource.of("QUANDL")
    val YAHOO: ObservableSource = ObservableSource.of("YAHOO")
}