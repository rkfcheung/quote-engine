package com.rkfcheung.quoteengine.model.price

import com.opengamma.strata.data.ObservableSource
import com.rkfcheung.quoteengine.model.definition.PriceAttribute
import com.rkfcheung.quoteengine.model.definition.Resolution
import com.rkfcheung.quoteengine.model.ticker.Ticker
import com.rkfcheung.quoteengine.util.DateTimeUtil.asZonedDateTime
import java.time.LocalDate

data class CandlePrice(
    override val resolution: Resolution,
    override val parameterSource: ObservableSource,
    override val quoteDate: LocalDate,
    override val parameterCode: Ticker,
    override var open: Double,
    override var high: Double,
    override var low: Double,
    override var close: Double
) : MultipleValuePrice(
    resolution,
    parameterSource,
    quoteDate.asZonedDateTime(),
    parameterCode,
    close
), WithCandle {
    init {
        setAttributes()
    }

    override fun setAttributes() {
        if (open > 0.0) {
            setAttribute(PriceAttribute.OPEN, open)
        }
        if (high > 0.0) {
            setAttribute(PriceAttribute.HIGH, high)
        }
        if (low > 0.0) {
            setAttribute(PriceAttribute.LOW, low)
        }
        if (close > 0.0) {
            setAttribute(PriceAttribute.CLOSE, close)
        }
        if (adjClose > 0.0 && adjClose != close && adjClose != value) {
            setAttribute(PriceAttribute.ADJ_CLOSE, adjClose)
        }
        super.setAttributes()
    }
}