package com.rkfcheung.quoteengine.model.price

import com.opengamma.strata.basics.currency.Currency

interface WithCurrency {
    val currency: Currency
}