package com.rkfcheung.quoteengine.model.price.measurement

import com.influxdb.annotations.Column
import com.influxdb.annotations.Measurement
import com.opengamma.strata.data.ObservableSource
import com.rkfcheung.quoteengine.model.definition.MeasurementBasics
import com.rkfcheung.quoteengine.model.definition.Resolution
import java.time.Instant

@Measurement(name = MeasurementBasics.PRICE)
data class PriceCurrencyMeasurement(
    @Column(tag = true) override var code: String = "",
    @Column(timestamp = true) override var time: Instant = Instant.now(),
    @Column override var field: String = MeasurementBasics.VALUE,
    @Column override var value: Double = 0.0,
    @Column(tag = true) override var resolution: String = Resolution.NONE.name,
    @Column(tag = true) override var parameterSource: String = ObservableSource.NONE.name,
    @Column(tag = true) var currency: String? = null
) : PriceMeasurement(code, time, field, value, resolution, parameterSource)