package com.rkfcheung.quoteengine.model.price

import com.rkfcheung.quoteengine.util.UnitUtil

interface WithBidAsk {
    val bid: Double
    val ask: Double

    fun mid(): Double = UnitUtil.mid(bid, ask)
}