package com.rkfcheung.quoteengine.model.price

import com.opengamma.strata.basics.currency.FxRate
import com.opengamma.strata.data.ObservableSource
import com.rkfcheung.quoteengine.model.definition.PriceAttribute
import com.rkfcheung.quoteengine.model.definition.Resolution
import com.rkfcheung.quoteengine.model.ticker.ForexTicker
import com.rkfcheung.quoteengine.util.UnitUtil
import com.rkfcheung.quoteengine.util.UnitUtil.asPrecisionNum
import org.ta4j.core.Bar
import java.time.ZonedDateTime

data class ForexPrice(
    override val resolution: Resolution,
    override val parameterSource: ObservableSource,
    override val timestamp: ZonedDateTime,
    override val parameterCode: ForexTicker,
    override var bid: Double,
    override var ask: Double
) : MultipleValuePrice(
    resolution,
    parameterSource,
    timestamp,
    parameterCode,
    UnitUtil.mid(bid, ask),
    parameterCode.counter
), WithBidAsk {
    init {
        setAttributes()
    }

    override fun asBar(): Bar =
        super.asBar().apply {
            addPrice(bid.asPrecisionNum())
            addPrice(ask.asPrecisionNum())
        }

    fun asFxRate(): FxRate {
        return FxRate.of(parameterCode.pair, value)
    }

    override fun setAttributes() {
        if (bid > 0.0 && bid != value) {
            setAttribute(PriceAttribute.BID, bid)
        }
        if (ask > 0.0 && ask != value) {
            setAttribute(PriceAttribute.ASK, ask)
        }
        super.setAttributes()
    }
}