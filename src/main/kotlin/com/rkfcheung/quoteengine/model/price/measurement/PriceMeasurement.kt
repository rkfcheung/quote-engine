package com.rkfcheung.quoteengine.model.price.measurement

import com.influxdb.annotations.Column
import com.influxdb.annotations.Measurement
import com.opengamma.strata.calc.ColumnName
import com.opengamma.strata.data.ObservableSource
import com.rkfcheung.quoteengine.model.definition.MeasurementBasics
import com.rkfcheung.quoteengine.model.definition.Resolution
import com.rkfcheung.quoteengine.model.price.Price
import com.rkfcheung.quoteengine.model.price.SingleValuePrice
import com.rkfcheung.quoteengine.util.TickerUtil
import java.time.Instant
import java.time.ZoneId
import java.time.ZonedDateTime

@Measurement(name = MeasurementBasics.PRICE)
open class PriceMeasurement(
    @Column(tag = true) open var code: String = "",
    @Column(timestamp = true) open var time: Instant = Instant.now(),
    @Column open var field: String = MeasurementBasics.VALUE,
    @Column open var value: Double = 0.0,
    @Column(tag = true) open var resolution: String = Resolution.NONE.name,
    @Column(tag = true) open var parameterSource: String = ObservableSource.NONE.name
) {
    constructor(
        code: ColumnName,
        timestamp: ZonedDateTime,
        value: Double,
        resolution: Resolution,
        parameterSource: ObservableSource,
        field: String
    ) : this(code.name, timestamp.toInstant(), field, value, resolution.name, parameterSource.name)

    open fun asPrice(): Price = asSingleValuePrice()

    fun asSingleValuePrice(): SingleValuePrice {
        return SingleValuePrice(
            Resolution.valueOf(resolution),
            ObservableSource.of(parameterSource),
            time.atZone(ZoneId.systemDefault()),
            TickerUtil.asTicker(code),
            value
        )
    }
}