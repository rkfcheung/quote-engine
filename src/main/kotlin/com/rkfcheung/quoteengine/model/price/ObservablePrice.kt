package com.rkfcheung.quoteengine.model.price

import com.opengamma.strata.data.ObservableSource
import com.rkfcheung.quoteengine.model.definition.Resolution
import com.rkfcheung.quoteengine.model.ticker.Ticker
import java.time.ZonedDateTime

open class ObservablePrice(
    override val resolution: Resolution,
    override val parameterSource: ObservableSource,
    override val timestamp: ZonedDateTime,
    override val parameterCode: Ticker,
    override val value: Double
) : Price(resolution, parameterSource, timestamp.toLocalDate(), parameterCode)
