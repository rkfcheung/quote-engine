package com.rkfcheung.quoteengine.model.price

interface WithVolume {
    val volume: Double
}