package com.rkfcheung.quoteengine.model.price

import com.fasterxml.jackson.annotation.JsonBackReference
import com.influxdb.client.write.Point
import com.opengamma.strata.basics.currency.Currency
import com.opengamma.strata.data.ObservableSource
import com.rkfcheung.quoteengine.model.definition.MeasurementBasics
import com.rkfcheung.quoteengine.model.definition.PriceAttribute
import com.rkfcheung.quoteengine.model.definition.Resolution
import com.rkfcheung.quoteengine.model.ticker.Ticker
import com.rkfcheung.quoteengine.util.UnitUtil
import com.rkfcheung.quoteengine.util.UnitUtil.asPrecisionNum
import com.rkfcheung.quoteengine.util.UnitUtil.mid
import org.ta4j.core.Bar
import org.ta4j.core.BaseBar
import org.ta4j.core.num.PrecisionNum
import java.time.ZonedDateTime

open class MultipleValuePrice(
    override val resolution: Resolution,
    override val parameterSource: ObservableSource,
    override val timestamp: ZonedDateTime,
    override val parameterCode: Ticker,
    override val value: Double,
    @JsonBackReference override val currency: Currency = Currency.XXX,
) : ObservablePrice(resolution, parameterSource, timestamp, parameterCode, value), WithCurrency {
    private val attributes = mutableMapOf<PriceAttribute, Double>()

    open var bid: Double
        set(value) = setAttribute(PriceAttribute.BID, value)
        get() = getAttribute(PriceAttribute.BID, value)

    open var ask: Double
        set(value) = setAttribute(PriceAttribute.ASK, value)
        get() = getAttribute(PriceAttribute.ASK, value)

    open var high: Double
        set(value) = setAttribute(PriceAttribute.HIGH, value)
        get() = getAttribute(PriceAttribute.HIGH, value)

    open var low: Double
        set(value) = setAttribute(PriceAttribute.LOW, value)
        get() = getAttribute(PriceAttribute.LOW, value)

    open var open: Double
        set(value) = setAttribute(PriceAttribute.OPEN, value)
        get() = getAttribute(PriceAttribute.OPEN, value)

    open var close: Double
        set(value) = setAttribute(PriceAttribute.CLOSE, value)
        get() = getAttribute(PriceAttribute.CLOSE, value)

    open var adjClose: Double
        set(value) = setAttribute(PriceAttribute.ADJ_CLOSE, value)
        get() = getAttribute(PriceAttribute.ADJ_CLOSE, close)

    open var volume: Double
        set(value) = setAttribute(PriceAttribute.VOLUME, value)
        get() = getAttribute(PriceAttribute.VOLUME, 0.0)

    var turnover: Double
        set(value) = setAttribute(PriceAttribute.TURNOVER, value)
        get() = getAttribute(PriceAttribute.TURNOVER, 0.0)

    open fun clone(newCurrency: Currency): MultipleValuePrice {
        val cur = if (newCurrency != Currency.XXX) newCurrency else currency
        return MultipleValuePrice(resolution, parameterSource, timestamp, parameterCode, value, cur).apply {
            setAttributes()
        }
    }

    override fun asBar(): Bar =
        BaseBar(
            resolution.asDuration(),
            timestamp,
            open.asPrecisionNum(),
            high.asPrecisionNum(),
            low.asPrecisionNum(),
            close.asPrecisionNum(),
            PrecisionNum.valueOf(volume * UnitUtil.K),
            PrecisionNum.valueOf(quasiTurnover() * UnitUtil.K)
        )

    override fun asPoint(measurement: String): Point {
        setAttributes()
        return super.asPoint(measurement)
    }

    override fun getMeasurementFields(): Map<String, Double> {
        return attributes.filter { it.value != 0.0 }.mapKeys { it.key.name } + super.getMeasurementFields()
    }

    override fun getMeasurementTags(): Map<String, String> {
        return if (currency != Currency.XXX) {
            mapOf(MeasurementBasics.CURRENCY to currency.code)
        } else {
            emptyMap()
        } + super.getMeasurementTags()
    }

    fun asCandlePrice(): CandlePrice =
        CandlePrice(resolution, parameterSource, quoteDate, parameterCode, open, high, low, close)

    fun asCandleVPrice(): CandleVPrice =
        CandleVPrice(resolution, parameterSource, quoteDate, parameterCode, open, high, low, close, volume, currency)

    fun hasAttribute(attribute: PriceAttribute): Boolean = attributes[attribute] ?: 0.0 > 0.0

    fun quasiTurnover(): Double = if (turnover > 0.0) {
        turnover
    } else {
        volume * mid(high, low)
    }

    protected open fun setAttributes() {
        if (volume > 0.0) {
            setAttribute(PriceAttribute.VOLUME, volume)
        }
        if (turnover > 0.0) {
            setAttribute(PriceAttribute.TURNOVER, turnover)
        }
    }

    protected fun setAttribute(attribute: PriceAttribute, value: Double) {
        attributes[attribute] = value
    }

    private fun getAttribute(attribute: PriceAttribute, defaultValue: Double = 0.0): Double {
        return attributes[attribute] ?: defaultValue
    }
}