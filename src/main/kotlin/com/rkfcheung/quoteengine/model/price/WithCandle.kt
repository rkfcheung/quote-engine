package com.rkfcheung.quoteengine.model.price

interface WithCandle {
    val open: Double
    val high: Double
    val low: Double
    val close: Double
}