package com.rkfcheung.quoteengine.model.price

import com.fasterxml.jackson.annotation.JsonIgnore
import com.influxdb.client.write.Point
import com.opengamma.strata.collect.timeseries.LocalDateDoublePoint
import com.opengamma.strata.data.FieldName
import com.opengamma.strata.data.ObservableSource
import com.opengamma.strata.market.observable.QuoteId
import com.rkfcheung.quoteengine.model.definition.MeasurementBasics
import com.rkfcheung.quoteengine.model.definition.Resolution
import com.rkfcheung.quoteengine.model.price.measurement.PriceMeasurement
import com.rkfcheung.quoteengine.model.ticker.Ticker
import com.rkfcheung.quoteengine.util.UnitUtil.asPrecisionNum
import org.ta4j.core.Bar
import org.ta4j.core.BaseBar
import org.ta4j.core.num.PrecisionNum
import java.time.LocalDate
import java.time.ZonedDateTime

abstract class Price(
    open val resolution: Resolution,
    open val parameterSource: ObservableSource,
    open val quoteDate: LocalDate,
    open val parameterCode: Ticker
) {
    abstract val value: Double
    abstract val timestamp: ZonedDateTime

    open fun asMeasurement(): PriceMeasurement {
        return PriceMeasurement(
            parameterCode.asMeasurementCode(),
            timestamp,
            value,
            resolution,
            parameterSource,
            MeasurementBasics.VALUE
        )
    }

    open fun asBar(): Bar =
        BaseBar(resolution.asDuration(), timestamp) { PrecisionNum.valueOf(it) }.apply {
            addPrice(value.asPrecisionNum())
        }

    open fun asPoint(measurement: String = MeasurementBasics.PRICE): Point {
        return Point.measurement(measurement)
            .time(timestamp.toInstant(), MeasurementBasics.WRITE_PRECISION)
            .addFields(getMeasurementFields())
            .addTags(getMeasurementTags())
    }

    open fun fieldName(): FieldName = FieldName.MARKET_VALUE

    @JsonIgnore
    open fun getMeasurementFields(): Map<String, Double> = mapOf(MeasurementBasics.VALUE to value)

    @JsonIgnore
    open fun getMeasurementTags(): Map<String, String> = mapOf(
        MeasurementBasics.CODE to parameterCode.asMeasurementCode(),
        MeasurementBasics.RESOLUTION to resolution,
        MeasurementBasics.PARAMETER_SOURCE to parameterSource
    ).mapValues { it.value.toString() }

    fun asLocalDateDoublePoint(): LocalDateDoublePoint {
        return LocalDateDoublePoint.of(quoteDate, value)
    }

    fun quoteId(): QuoteId = QuoteId.of(parameterCode.standardId(), fieldName(), parameterSource)
}